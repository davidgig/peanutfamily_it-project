package ch.fhnw.peanutfamily.client;

import ch.fhnw.peanutfamily.resources.LanguageTranslator;
import ch.fhnw.peanutfamily.resources.PropertyControl;
import ch.fhnw.peanutfamily.resources.templates.View;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * The GameBoard View is the UI for the Visible GameBoard.
 * It contains all the needed Pane's, Buttons, labels etc. 
 * 
 * At the end we also set all CSS Formats in this class and load it. 
 * 
 * 
 * @author roger
 *
 */
public class GameBoardView extends View<GameBoardModel> {

	private LanguageTranslator languageTranslator;

	private Scene scene;

	// Defining MenuBar
	public MenuBar menuBar;
	public Menu menuFile;
	public Menu menuLanguage;
	public Menu menuGame;

	public MenuItem menuLanguageDe;
	public MenuItem menuLanguageEn;
	public MenuItem menuClose;
	public MenuItem menuQuit;

	// General Border Pane
	public BorderPane board_pane;

	// PlayBord Center
	public GridPane gameBoardPane;

	// Right Side VBox Panes
	public VBox gameBoardUtilBox;
	public HBox myCardsPane;
	public ScrollPane myCardsOuterPane;
	public HBox myTilesPane;
	public ScrollPane myTilesOuterPane;
	public GridPane moveBtnPane;

	// Right Side Label
	public Label playerCardsLbl;
	public Label playerTilesLbl;

	// Controll Buttons
	public Button buyCardsBtn;
	public Button makeMoveBtn;
	public Button playCardBtn;
	public Button endRoundBtn;
	public Button rejectMoveBtn;
	public Button buildBridgeBtn;
	public Button buyBoatRideBtn;

	// Bottom
	public HBox bottomBox;
	public GridPane chatPane;
	public GridPane scorePane;

	// Chat Box
	public TextField chatTxt;
	public Button chatSendBtn;
	public TextArea chatArea;

	// Score Pane
	public Label pointsLbl;
	public Label tilesLbl;
	public Label usernameLbl;
	public Label player2Lbl;
	public Label player3Lbl;
	public Label points1;
	public Label points2;
	public Label points3;
	public Label tiles1;
	public Label tiles2;
	public Label tiles3;

	public GameBoardView(Stage stage, GameBoardModel model) {
		super(stage, model);

		languageTranslator = PropertyControl.getPropertyControl().getLanguageTranslator();
	}
	
	
	
	//The method to initialize the UI for the Game. 
	@Override
	protected void initUI() {

		// Defining Menu Items for menuBar
		menuBar = new MenuBar();
		menuFile = new Menu(languageTranslator.getTranslation("general.menu.menuFile"));
		menuLanguage = new Menu(languageTranslator.getTranslation("general.menu.menuLanguage"));

		menuLanguageDe = new MenuItem(languageTranslator.getTranslation("general.menu.menuLanguageDe"));
		menuLanguageDe.setOnAction(event -> {
			PropertyControl.getPropertyControl().setLanguageTranslator(new LanguageTranslator("DE"));
			updateLanguage();
		});

		menuLanguageEn = new MenuItem(languageTranslator.getTranslation("general.menu.menuLanguageEn"));
		menuLanguageEn.setOnAction(event -> {
			PropertyControl.getPropertyControl().setLanguageTranslator(new LanguageTranslator("EN"));
			updateLanguage();
		});

		menuGame = new Menu(languageTranslator.getTranslation("general.menu.menuGame"));
		menuQuit = new MenuItem(languageTranslator.getTranslation("general.menu.menuQuit"));

		menuLanguage.getItems().addAll(menuLanguageDe, menuLanguageEn);

		menuClose = new MenuItem(languageTranslator.getTranslation("general.menu.menuClose"));
		menuClose.setOnAction(event -> {
			stage.close();
		});
		menuFile.getItems().addAll(menuLanguage, menuClose);
		menuGame.getItems().addAll(menuQuit);
		menuBar.getMenus().addAll(menuFile, menuGame);

		// Game GridPane
		gameBoardPane = new GridPane();

		// Chat Box
		chatPane = new GridPane();
		chatArea = new TextArea();
		chatArea.textProperty().bind(model.chatTextProperty());
		chatTxt = new TextField("");
		chatSendBtn = new Button(languageTranslator.getTranslation("chat.buttons.chatSendBtn"));
		chatPane.add(chatArea, 0, 0, 2, 1);
		chatPane.add(chatTxt, 0, 1);
		chatPane.add(chatSendBtn, 1, 1);

		// MyCards Pane
		myCardsPane = new HBox();
		myCardsOuterPane = new ScrollPane();
		myCardsOuterPane.setContent(myCardsPane);

		// MyTiles Pane
		myTilesPane = new HBox();
		myTilesOuterPane = new ScrollPane();
		myTilesOuterPane.setContent(myTilesPane);

		// MoveBtn Pane
		makeMoveBtn = new Button(languageTranslator.getTranslation("game.buttons.makeMoveBtn"));
		buildBridgeBtn = new Button(languageTranslator.getTranslation("game.buttons.buildBridgeBtn"));
		buyCardsBtn = new Button(languageTranslator.getTranslation("game.buttons.buyCardsBtn"));
		buyBoatRideBtn = new Button(languageTranslator.getTranslation("game.buttons.buyBoatRideBtn"));
		playCardBtn = new Button(languageTranslator.getTranslation("game.buttons.playCardBtn"));
		rejectMoveBtn = new Button(languageTranslator.getTranslation("game.buttons.rejectMoveBtn"));
		endRoundBtn = new Button(languageTranslator.getTranslation("game.buttons.endRoundBtn"));

		moveBtnPane = new GridPane();
		moveBtnPane.add(makeMoveBtn, 0, 0);
		moveBtnPane.add(buyCardsBtn, 1, 0);
		moveBtnPane.add(playCardBtn, 0, 1);
		moveBtnPane.add(rejectMoveBtn, 1, 1);
		moveBtnPane.add(endRoundBtn, 0, 2);
		moveBtnPane.add(buyBoatRideBtn, 1, 2);

		// Labels
		playerCardsLbl = new Label(languageTranslator.getTranslation("game.label.playerCardsLbl"));
		playerTilesLbl = new Label(languageTranslator.getTranslation("game.label.playerTilesLbl"));

		// GameBoardUtil Box
		gameBoardUtilBox = new VBox();
		gameBoardUtilBox.getChildren().addAll(playerCardsLbl, myCardsOuterPane, playerTilesLbl, myTilesOuterPane,
				moveBtnPane);

		// Bottom Box
		bottomBox = new HBox();
		bottomBox.getChildren().addAll(chatPane);

		board_pane = new BorderPane();
		board_pane.setTop(menuBar);
		board_pane.setRight(gameBoardUtilBox);
		board_pane.setCenter(gameBoardPane);
		board_pane.setBottom(bottomBox);

		scene = new Scene(board_pane, 1200, 620);

		setCSS();
	}

	@Override
	public void createUI() {

		stage.setTitle(languageTranslator.getTranslation("general.name"));
		stage.setScene(this.scene);
		stage.setResizable(true);
	}

	/** 
	 * This method updates the language of all texts. It is called by the button in the menue when the new language is selected.
	 * 
	 * @author Roger
	 */
	public void updateLanguage() {

		LanguageTranslator manager = PropertyControl.getPropertyControl().getLanguageTranslator();
		

		// Update Title
		stage.setTitle(manager.getTranslation("general.name"));

		// Update Menu
		menuFile.setText(manager.getTranslation("general.menu.menuFile"));
		menuLanguage.setText(manager.getTranslation("general.menu.menuLanguage"));
		menuLanguageDe.setText(manager.getTranslation("general.menu.menuLanguageDe"));
		menuLanguageEn.setText(manager.getTranslation("general.menu.menuLanguageEn"));
		menuClose.setText(manager.getTranslation("general.menu.menuClose"));
		menuGame.setText(manager.getTranslation("general.menu.menuGame"));
		menuQuit.setText(manager.getTranslation("general.menu.menuQuit"));

		// Update all Labels
		playerCardsLbl.setText(manager.getTranslation("game.label.playerCardsLbl"));
		playerTilesLbl.setText(manager.getTranslation("game.label.playerTilesLbl"));

		// Update all Buttons
		makeMoveBtn.setText(manager.getTranslation("game.buttons.makeMoveBtn"));
		buildBridgeBtn.setText(manager.getTranslation("game.buttons.buildBridgeBtn"));
		buyCardsBtn.setText(manager.getTranslation("game.buttons.buyCardsBtn"));
		buyBoatRideBtn.setText(manager.getTranslation("game.buttons.buyBoatRideBtn"));
		playCardBtn.setText(manager.getTranslation("game.buttons.playCardBtn"));
		rejectMoveBtn.setText(manager.getTranslation("game.buttons.rejectMoveBtn"));
		endRoundBtn.setText(manager.getTranslation("game.buttons.endRoundBtn"));
	}

	/**
	 * This method sets styles in CSS format for all nodes, buttons, panes, labes etc
	 * 
	 * @author Roger
	 */
	public void setCSS() {

		// GameBoard Background
		BackgroundImage image = new BackgroundImage(new Image(getClass().getResourceAsStream("variante10.png")),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);
		board_pane.setBackground(new Background(image));

		// GameBoard
		gameBoardPane.setPadding(new Insets(10, 10, 10, 10));

		// ChatBox
		chatArea.setPrefHeight(80);
		chatArea.setPrefWidth(600);
		chatTxt.setPrefWidth(450);
		chatSendBtn.setPrefWidth(150);
		chatPane.setVgap(3);
		chatPane.setHgap(3);
		chatPane.setMaxWidth(610);
		chatPane.setPadding(new Insets(10, 10, 10, 10));
		
		// Controll Buttons
		buyCardsBtn.setPrefSize(150, 30);
		buyCardsBtn.setStyle("-fx-base: #336699");
		makeMoveBtn.setPrefSize(150, 30);
		makeMoveBtn.setStyle("-fx-base: #336699");
		playCardBtn.setPrefSize(150, 30);
		playCardBtn.setStyle("-fx-base: #336699");
		endRoundBtn.setPrefSize(150, 30);
		endRoundBtn.setStyle("-fx-base: #336699");
		rejectMoveBtn.setPrefSize(150, 30);
		rejectMoveBtn.setStyle("-fx-base: #336699");
		buildBridgeBtn.setPrefSize(150, 30);
		buildBridgeBtn.setStyle("-fx-base: #336699");
		buyBoatRideBtn.setPrefSize(150, 30);
		buyBoatRideBtn.setStyle("-fx-base: #336699");
		moveBtnPane.setPadding(new Insets(10, 0, 0, 0));
		moveBtnPane.setVgap(5);
		moveBtnPane.setHgap(2);
		
		// MyCards
		playerCardsLbl.setTextFill(Color.WHITE);

		myCardsOuterPane.setFitToHeight(true);
		myCardsOuterPane.setPrefHeight(100);

		// MyTiles
		playerTilesLbl.setTextFill(Color.WHITE);

		myTilesOuterPane.setFitToHeight(true);
		myTilesOuterPane.setPrefHeight(100);

		// GameBoardUtil Box
		gameBoardUtilBox.setPadding(new Insets(10, 10, 10, 10));
		gameBoardUtilBox.setPrefWidth(300);
	}
}
