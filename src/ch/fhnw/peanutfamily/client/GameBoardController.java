package ch.fhnw.peanutfamily.client;

import java.util.ArrayList;
import java.util.List;

import com.sun.prism.paint.Color;

import ch.fhnw.peanutfamily.client.communication.ClientGameLogic;
import ch.fhnw.peanutfamily.client.communication.ClientSender;
import ch.fhnw.peanutfamily.common.Card;
import ch.fhnw.peanutfamily.common.Figure;
import ch.fhnw.peanutfamily.common.GameBoard;
import ch.fhnw.peanutfamily.common.GameResult;
import ch.fhnw.peanutfamily.common.IntermediateResult;
import ch.fhnw.peanutfamily.common.MakeAMove;
import ch.fhnw.peanutfamily.common.PlayerInfo;
import ch.fhnw.peanutfamily.common.Tile;
import ch.fhnw.peanutfamily.common.Tile.TileNumber;
import ch.fhnw.peanutfamily.common.TileStack;
import ch.fhnw.peanutfamily.common.TransferObject;
import ch.fhnw.peanutfamily.common.TransferObject.PlayerStatus;
import ch.fhnw.peanutfamily.common.TransferObject.TransferType;
import ch.fhnw.peanutfamily.resources.LanguageTranslator;
import ch.fhnw.peanutfamily.resources.PropertyControl;
import ch.fhnw.peanutfamily.resources.templates.Controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

/**
 * The GameBoardController handles all the Button Actions in the game. It also
 * contains the methods to generate the game board, tiles, cards, player
 * figures.
 * 
 * In every method we call the language Translator to get the current language settings of the game
 * this allows to change the languge while playing.
 * 
 * @author David and (Roger)
 *
 */
public class GameBoardController extends Controller<GameBoardView, GameBoardModel> {

	private ClientGameLogic clientGameLogic;
	private LanguageTranslator languageTranslator;

	/**
	 * The GameBoardController starts the view and creates EventHandler for all
	 * buttons and game subjects (cards, tiles...)
	 * 
	 * @param view
	 *            the View class
	 * @param model
	 *            the Model class
	 * 
	 * @author David
	 */
	public GameBoardController(GameBoardView view, GameBoardModel model) {
		super(view, model);

		view.startUI();

		languageTranslator = PropertyControl.getPropertyControl().getLanguageTranslator();

		showMessage(languageTranslator.getTranslation("game.messages.welcome"));

		// sends the chat text message to all other players via the server
		view.chatSendBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				String message = view.chatTxt.getText();
				view.chatTxt.setText("");

				if (!message.equals("")) {
					TransferObject transferObject = new TransferObject(TransferType.MESSAGE);
					transferObject.setTransferObject(message);

					ClientSender.send(transferObject);
				}
			}
		});

		/*
		 * After selecting a card and a figure its send the information to the
		 * server to make the move
		 */
		view.makeMoveBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				String selectedFigure = model.getSelectedFigure();
				List<String> selectedCards = model.getSelectedCards();

				if (model.getPlayerStatus() != PlayerStatus.READY) {

					showMessage(languageTranslator.getTranslation("game.messages.pleaseWait"));
				} else if (selectedFigure.equals("")) {

					showMessage(languageTranslator.getTranslation("game.messages.selectFigure"));
				} else if (selectedCards.size() != 1) {

					showMessage(languageTranslator.getTranslation("game.messages.selectOneCard"));
				} else {

					MakeAMove makeAMove = new MakeAMove();
					makeAMove.setGameId(model.getGame().getId());
					makeAMove.setPlayerId(model.getPlayer().getId());
					makeAMove.setSelectedCard(selectedCards.get(0));
					makeAMove.setSelectedFigure(selectedFigure);

					model.getSelectedCards().clear();
					model.getSelectedTiles().clear();

					TransferObject transferObject = new TransferObject(TransferType.MAKEAMOVE);
					transferObject.setTransferObject(makeAMove);

					ClientSender.send(transferObject);

					showMessage("Move was played");
				}
			}
		});

		/*
		 * With this button we can inform the server that my round is finished
		 */
		view.endRoundBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				if (model.getPlayerStatus() != PlayerStatus.READY && model.getPlayerStatus() != PlayerStatus.MOVEMADE) {

					showMessage(languageTranslator.getTranslation("game.messages.notPossible"));
				} else {

					PlayerInfo playerInfo = new PlayerInfo();
					playerInfo.setGame(model.getGame());
					playerInfo.setPlayer(model.getPlayer());

					TransferObject transferObject = new TransferObject(TransferType.ENDROUND);
					transferObject.setTransferObject(playerInfo);

					ClientSender.send(transferObject);

					model.setPlayerStatus(PlayerStatus.WAIT);

					model.setSelectedCards(new ArrayList<>());
					model.setSelectedTiles(new ArrayList<>());
					showMessage("Round is ended");
				}
			}
		});

		/*
		 * After getting a notification that the target field is occupied we can
		 * play another move by selecting a second card
		 */
		view.playCardBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				List<String> selectedCards = model.getSelectedCards();

				// check if card is already played
				boolean isPlayed = false;
				for (String selectedCard : selectedCards) {
					for (String cardId : model.getIntermediateResult().getPlayedCars()) {
						if (cardId.equals(selectedCard)) {
							isPlayed = true;
						}
					}
				}

				if (model.getPlayerStatus() != PlayerStatus.INTERMEDIATEOCCUPIED) {

					showMessage(languageTranslator.getTranslation("game.messages.notPossible"));
				} else if (0 > selectedCards.size() || 1 < selectedCards.size()) {

					showMessage(languageTranslator.getTranslation("game.messages.selectOneCard"));
				} else if (isPlayed) {

					showMessage(languageTranslator.getTranslation("game.messages.cardAlreadyPlayed"));
				} else {

					IntermediateResult intermediateResult = model.getIntermediateResult();
					intermediateResult.setSelectedCards(selectedCards);

					TransferObject transferObject = new TransferObject(TransferType.INTERMEDIATERESULT);
					transferObject.setTransferObject(intermediateResult);

					ClientSender.send(transferObject);
				}
			}
		});

		/*
		 * Moves like playing a second card or buying to cross the water can be
		 * reject if the player doesnt want to do it or he doesnt can
		 */
		view.rejectMoveBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				if (model.getPlayerStatus() != PlayerStatus.INTERMEDIATECOSTS
						&& model.getPlayerStatus() != PlayerStatus.INTERMEDIATEOCCUPIED) {

					showMessage(languageTranslator.getTranslation("game.messages.notPossible"));
				} else {
					model.setIntermediateResult(null);
					model.setPlayerStatus(PlayerStatus.READY);

					showMessage("The move was rejected, play something else.");
				}
			}
		});

		/*
		 * The player can exchange a tile for cards (half the value of the tile)
		 */
		view.buyCardsBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				List<String> selectedTiles = model.getSelectedTiles();

				if (model.getPlayerStatus() != PlayerStatus.READY) {

					showMessage(languageTranslator.getTranslation("game.messages.notPossible"));
				} else if (selectedTiles.size() == 0) {

					showMessage(languageTranslator.getTranslation("game.messages.selectOneTile"));
				} else {
					PlayerInfo playerInfo = new PlayerInfo();
					playerInfo.setGame(model.getGame());
					playerInfo.setPlayer(model.getPlayer());
					playerInfo.setSelectedTile(selectedTiles.get(0));

					TransferObject transferObject = new TransferObject(TransferType.BUYCARD);
					transferObject.setTransferObject(playerInfo);

					ClientSender.send(transferObject);

					model.setSelectedCards(new ArrayList<>());
					model.setSelectedTiles(new ArrayList<>());
				}
			}
		});

		/*
		 * To cross the water the player has to pay the amount which is needed
		 * with cards and tiles otherwise he has to reject and play something
		 * else. Before he can cross the water the function checks if he pays
		 * enough
		 */
		view.buyBoatRideBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				List<String> selectedTiles = model.getSelectedTiles();
				List<String> selectedCards = model.getSelectedCards();

				// calculate cards value
				int cardsValue = selectedCards.size();

				// calculate tiles value
				int tilesValue = 0;
				for (String selectedTile : selectedTiles) {
					for (Tile tile : model.getMyTiles()) {
						if (selectedTile.equals(tile.getId())) {
							tilesValue += tile.getTileNumber().getValue();
						}
					}
				}

				if (model.getPlayerStatus() != PlayerStatus.INTERMEDIATECOSTS) {

					showMessage(languageTranslator.getTranslation("game.messages.notPossible"));
				} else if (model.getIntermediateResult().getCosts() > (cardsValue + tilesValue)) {

					showMessage(languageTranslator.getTranslation("game.messages.costsToHigh"));
				} else {

					IntermediateResult intermediateResult = model.getIntermediateResult();
					intermediateResult.setSelectedCards(selectedCards);
					intermediateResult.setSelectedTiles(selectedTiles);

					TransferObject transferObject = new TransferObject(TransferType.INTERMEDIATERESULT);
					transferObject.setPlayerStatus(PlayerStatus.INTERMEDIATECOSTS);
					transferObject.setTransferObject(intermediateResult);

					ClientSender.send(transferObject);

					showMessage("Boat ride was payed");
				}
			}
		});
	}

	/**
	 * This function build the whole GameBoard with all its game subjects
	 * 
	 * @param gameBoard
	 *            the GameBoard object containing all game subjects
	 * @param playerStatus
	 *            the new PlayerStatus
	 * 
	 * @author David
	 */
	public void initGameBoard(GameBoard gameBoard, PlayerStatus playerStatus) {

		// store all relevant game subjects
		model.setTileStackes(gameBoard.getStacks());
		model.setMyCards(gameBoard.getPlayerHands().get(0).getMyCards());
		model.setMyTiles(gameBoard.getPlayerHands().get(0).getMyTiles());
		model.setFigures(gameBoard.getFigures());
		model.setGame(gameBoard.getGame());
		model.setPlayer(gameBoard.getPlayerHands().get(0).getPlayer());

		// clean current game subjects
		cleanCardHand();
		cleanTileHand();
		cleanGameBoard();

		// build new game subjects
		buildGameBoard();
		buildCardHand();
		buildTileHand();

		model.setPlayerStatus(playerStatus);
	}

	/**
	 * This function creates all cards for the PlayerHand
	 * 
	 * @author David
	 */
	public void buildCardHand() {

		int count = 0;

		for (Card card : model.getMyCards()) {
			buildCard(card, count);
			count++;
		}
	}

	/**
	 * This function creates all tiles for the playerHand
	 * 
	 * @author David
	 */
	public void buildTileHand() {

		int count = 0;

		for (Tile tile : model.getMyTiles()) {
			buildTile(tile, count);
			count++;
		}
	}

	/**
	 * This method build the whole GameBoard(fields), in the whole 55 pieces
	 * (atlantis, water, tiles, land)
	 * 
	 * @author David
	 */
	public void buildGameBoard() {

		TileStack tileStack = null;
		int count = 0;

		// first column with atlantis
		for (int i = 0; i <= 6; i++) {
			tileStack = model.getTileStackes().get(count);
			buildField(tileStack, 0, i);
			count++;
		}

		// fist connection part
		tileStack = model.getTileStackes().get(7);
		buildField(tileStack, 1, 6);
		count++;

		// second column
		for (int i = 6; i >= 0; i--) {
			tileStack = model.getTileStackes().get(count);
			buildField(tileStack, 2, i);
			count++;
		}

		// second connection part
		tileStack = model.getTileStackes().get(15);
		buildField(tileStack, 3, 0);
		count++;

		// third column
		for (int i = 0; i <= 6; i++) {
			tileStack = model.getTileStackes().get(count);
			buildField(tileStack, 4, i);
			count++;
		}

		// third connection part
		tileStack = model.getTileStackes().get(23);
		buildField(tileStack, 5, 6);
		count++;

		// fourth column
		for (int i = 6; i >= 0; i--) {
			tileStack = model.getTileStackes().get(count);
			buildField(tileStack, 6, i);
			count++;
		}

		// fourth connection part
		tileStack = model.getTileStackes().get(31);
		buildField(tileStack, 7, 0);
		count++;

		// fifth column
		for (int i = 0; i <= 6; i++) {
			tileStack = model.getTileStackes().get(count);
			buildField(tileStack, 8, i);
			count++;
		}

		// fifth connection part
		tileStack = model.getTileStackes().get(39);
		buildField(tileStack, 9, 6);
		count++;

		// sixth column - with land
		for (int i = 6; i >= 0; i--) {
			tileStack = model.getTileStackes().get(count);
			buildField(tileStack, 10, i);
			count++;
		}

		// sixth connection part
		tileStack = model.getTileStackes().get(47);
		buildField(tileStack, 11, 0);
		count++;

		// seventh column . with atlantis
		for (int i = 0; i <= 6; i++) {
			tileStack = model.getTileStackes().get(count);
			buildField(tileStack, 12, i);
			count++;
		}
	}

	/**
	 * This method creates a real card subject
	 * 
	 * @param card
	 *            the Card object which will be shown
	 * @param col
	 *            the column to place the card
	 * 
	 * @author David
	 */
	public void buildCard(Card card, int col) {

		String name = card.getCardColor().toString().toLowerCase();

		// background image
		Image image = new Image(getClass().getClassLoader()
				.getResourceAsStream("ch/fhnw/peanutfamily/client/images/card_" + name + ".jpg"));
		ImageView imageView = new ImageView(image);
		imageView.setFitWidth(40);
		imageView.setFitHeight(60);

		Button newCard = new Button();
		newCard.setGraphic(imageView);
		newCard.setId(card.getId());

		// handler to select or unselect the card
		newCard.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				String selectedCard = ((Control) e.getSource()).getId();

				if (model.getSelectedCards().contains(selectedCard)) {
					model.getSelectedCards().remove(selectedCard);
					((Control) e.getSource()).setStyle("-fx-border: 0px;");
				} else {
					model.getSelectedCards().add(selectedCard);
					((Control) e.getSource()).setStyle("-fx-border: 2px solid; -fx-border-color: black;");
				}

			}
		});

		view.myCardsPane.getChildren().add(newCard);
	}

	/**
	 * This method creates a real tile subject
	 * 
	 * @param tile
	 *            the Tile object which will be shown
	 * @param col
	 *            the column where to place the card
	 * 
	 * @author David
	 */
	public void buildTile(Tile tile, int col) {

		String nameColor = tile.getTileColor().toString().toLowerCase();
		int nameNumber = tile.getTileNumber().getValue();

		// background image
		Image image = new Image(getClass().getClassLoader()
				.getResourceAsStream("ch/fhnw/peanutfamily/client/images/" + nameColor + "_" + nameNumber + ".jpg"));
		ImageView imageView = new ImageView(image);
		imageView.setFitWidth(60);
		imageView.setFitHeight(60);

		Button newTile = new Button();
		newTile.setGraphic(imageView);
		newTile.setId(tile.getId());

		// handler to select or unselect the tile
		newTile.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				String selectedTile = ((Control) e.getSource()).getId();

				if (model.getSelectedTiles().contains(selectedTile)) {
					model.getSelectedTiles().remove(selectedTile);
					((Control) e.getSource()).setStyle("-fx-border: 0px;");
				} else {
					model.getSelectedTiles().add(selectedTile);
					((Control) e.getSource()).setStyle("-fx-border: 2px solid; -fx-border-color: black;");
				}
			}
		});

		view.myTilesPane.getChildren().add(newTile);
	}

	/**
	 * This method creates the real figure subject
	 * 
	 * @param figure
	 *            the Figure to create
	 * @return a button with the desing of a figure
	 * 
	 * @author David
	 */
	public Button buildFigure(Figure figure) {

		languageTranslator = PropertyControl.getPropertyControl().getLanguageTranslator();

		Button btnFigure = new Button();
		btnFigure.setPrefHeight(10);
		btnFigure.setPrefWidth(10);
		btnFigure.setId(figure.getId());
		btnFigure.setStyle("-fx-background-color: " + figure.getColor());

		// handler to select the figure and check if it is mine
		btnFigure.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				boolean isMyFigure = false;

				// check if my figure
				for (Figure figure : model.getFigures()) {
					if (figure.getId().equals(((Control) e.getSource()).getId())) {
						if (figure.getFkPlayer().equals(model.getPlayer().getId())) {
							isMyFigure = true;
						}
					}
				}

				if (isMyFigure) {
					model.setSelectedFigure(((Control) e.getSource()).getId());
				} else {
					showMessage(languageTranslator.getTranslation("game.messages.selectYourFigure"));
				}
			}
		});

		return btnFigure;
	}

	/**
	 * This method builds the TileStack (fields) of the GameBoard
	 * 
	 * @param tileStack
	 *            the TileStack which is to be build
	 * @param row
	 *            the row where to place it
	 * @param col
	 *            the column where to place it
	 * 
	 * @author David
	 */
	public void buildField(TileStack tileStack, int row, int col) {

		languageTranslator = PropertyControl.getPropertyControl().getLanguageTranslator();

		String name = "";

		if (tileStack.isAtlantis()) {

			name = "atlantis";
		} else if (tileStack.isLand()) {

			name = "land";
		} else if (tileStack.isWater()) {

			name = "water";
		} else {

			Tile topTile = tileStack.getTiles().get(0);

			String color = topTile.getTileColor().toString();
			int number = topTile.getTileNumber().getValue();
			name = color.toLowerCase() + "_" + number;
		}

		// background image
		Image image = new Image(
				getClass().getClassLoader().getResourceAsStream("ch/fhnw/peanutfamily/client/images/" + name + ".jpg"));
		ImageView imageView = new ImageView(image);
		imageView.setFitWidth(60);
		imageView.setFitHeight(60);

		StackPane stackPane = new StackPane();
		stackPane.getChildren().add(imageView);

		// set figure if there is one
		if (tileStack.isAtlantis()) {

			GridPane atlantisPane = new GridPane();

			// show all Figures of all players which are still at Atlantis in a
			// grid
			for (Figure figure : model.getFigures()) {

				if (tileStack.getPosition() == figure.getTileStackPosition()) {

					Button btnFigure = buildFigure(figure);

					if (figure.getColor().equals("red")) {
						atlantisPane.add(btnFigure, 0, 0);
					} else if (figure.getColor().equals("blue")) {
						atlantisPane.add(btnFigure, 0, 1);
					} else if (figure.getColor().equals("grey")) {
						atlantisPane.add(btnFigure, 1, 0);
					} else {
						atlantisPane.add(btnFigure, 1, 1);
					}
				}
			}

			stackPane.getChildren().add(atlantisPane);

		} else {

			for (Figure figure : model.getFigures()) {

				if (tileStack.getPosition() == figure.getTileStackPosition()) {

					Button btnFigure = buildFigure(figure);
					stackPane.getChildren().add(btnFigure);
				}
			}
		}

		view.gameBoardPane.add(stackPane, row, col);
	}

	/**
	 * This method removes all cards in the PlayerHand
	 * 
	 * @author David
	 */
	public void cleanCardHand() {
		view.myCardsPane.getChildren().clear();
	}

	/**
	 * This method removes all tiles in the PlayerHand
	 * 
	 * @author David
	 */
	public void cleanTileHand() {
		view.myTilesPane.getChildren().clear();
	}

	/**
	 * This method removes all tiles on the GameBoard
	 * 
	 * @author David
	 */
	public void cleanGameBoard() {
		view.gameBoardPane.getChildren().clear();
	}

	/**
	 * This method writes messages to the chat box
	 * 
	 * @param message
	 *            the text to show to the user
	 * 
	 * @author David
	 */
	public void showMessage(String message) {

		model.setChatText(model.getChatText() + "\n" + message);
		view.chatArea.positionCaret(view.chatArea.getLength());
	}

	/**
	 * This methods shows messages to the Player depending on the PlayerStaus
	 * 
	 * @param playerStatus
	 *            the current PlayerStatus (Ready, IntermediateOccupied,
	 *            IntermediateCosts)
	 * 
	 * @author David
	 */
	public void doPlayerStatus(PlayerStatus playerStatus) {

		languageTranslator = PropertyControl.getPropertyControl().getLanguageTranslator();

		model.setPlayerStatus(playerStatus);

		if (playerStatus == PlayerStatus.READY) {

			showMessage(languageTranslator.getTranslation("game.messages.yourTurn") + " ("
					+ model.getPlayer().getColor() + ")");
		}

		if (playerStatus == PlayerStatus.INTERMEDIATEOCCUPIED) {

			showMessage(languageTranslator.getTranslation("game.messages.occupiedField"));
		}

		if (playerStatus == PlayerStatus.INTERMEDIATECOSTS) {

			int costs = model.getIntermediateResult().getCosts();
			showMessage(languageTranslator.getTranslation("game.messages.payToCrossWater") + " " + costs);
		}
	}

	/**
	 * This method stores the IntermediateResult and informs the Player about
	 * the further actions
	 * 
	 * @param intermediateResult
	 *            the object containing all information of the current move
	 * @param playerStatus
	 *            the new PlayerStatus (IntermediateOccupied or
	 *            IntermediateCosts)
	 * 
	 * @author David
	 */
	public void initIntermediateResult(IntermediateResult intermediateResult, PlayerStatus playerStatus) {

		model.setPlayerStatus(playerStatus);
		model.setIntermediateResult(intermediateResult);

		doPlayerStatus(playerStatus);
	}

	/**
	 * This method informs the player about the game result
	 * 
	 * @param gameResult
	 * @param playerStatus
	 * 
	 * @author David
	 */
	public void finishGame(GameResult gameResult, PlayerStatus playerStatus) {

		if(gameResult.isWinner()) {
			showMessage(languageTranslator.getTranslation("game.messages.gameWinner"));
			
		} else {
			showMessage(languageTranslator.getTranslation("game.messages.gameNotWinner"));
		}
		showMessage(languageTranslator.getTranslation("game.messages.gameResultScore") + " " + gameResult.getScore());
		showMessage(languageTranslator.getTranslation("game.messages.endGame"));
		model.setPlayerStatus(playerStatus);
	}
}
