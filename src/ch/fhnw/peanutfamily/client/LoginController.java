package ch.fhnw.peanutfamily.client;

import ch.fhnw.peanutfamily.client.communication.ClientGameLogic;
import ch.fhnw.peanutfamily.client.communication.ClientSender;
import ch.fhnw.peanutfamily.common.TransferObject;
import ch.fhnw.peanutfamily.common.TransferObject.TransferType;
import ch.fhnw.peanutfamily.common.UserData;
import ch.fhnw.peanutfamily.resources.templates.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * 
 * @author Roger
 *
 */
public class LoginController extends Controller<LoginView, LoginModel> {

	private ClientGameLogic clientGameLogic;
	
	/**
	 * The LoginController starts the view and creates EventHandler for all
	 * buttons
	 * 
	 * @param view the View class
	 * @param model the Model class
	 * 
	 * @author Roger
	 */
	public LoginController(LoginView view, LoginModel model) {
		super(view, model);

		view.startUI();

		// Action for Login Button
		view.loginBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				String loginName = view.usernameTxt.getText().toString();
				String loginPW = view.passwordTxt.getText().toString();

				UserData login = new UserData(loginName, loginPW);
				TransferObject transferObject = new TransferObject(TransferType.LOGIN);
				transferObject.setTransferObject(login);
				ClientSender.send(transferObject);
			}
		});

		// Action for New User Button
		view.newUserBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				clientGameLogic.createRegisterMVC();
			}
		});
	}

	/**
	 * This method shows a login error messsage
	 * 
	 * @author Roger
	 */
	public void throwErrorMessage() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Login Error");
		alert.setHeaderText("login credentials are wrong");
		alert.show();
	}
	

	/**
	 * This methods sets the ClientGameLogic to have access to it (for changing pages)
	 * 
	 * @param clientGameLogic the ClientGameLogic
	 * 
	 * @author Roger
	 */
	public void setClientGameLogic(ClientGameLogic clientGameLogic) {
		
		this.clientGameLogic = clientGameLogic;
	}
}
