package ch.fhnw.peanutfamily.client;

import ch.fhnw.peanutfamily.common.Game;
import ch.fhnw.peanutfamily.common.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class LobbyModel {

	// attributes
	
	private User myUser = new User();

	private ObservableList<Game> newGames = FXCollections.observableArrayList();

	private String activeJoinGameId = "";
	private int activeAmountOfPlayers = 2;

	// getters/setters
	
	public User getMyUser() {
		return myUser;
	}

	public void setMyUser(User myUser) {
		this.myUser = myUser;
	}

	public ObservableList<Game> getNewGames() {
		return newGames;
	}

	public void setNewGames(ObservableList<Game> newGames) {
		this.newGames = newGames;
	}

	public String getActiveJoinGameId() {
		return activeJoinGameId;
	}

	public void setActiveJoinGameId(String activeJoinGameId) {
		this.activeJoinGameId = activeJoinGameId;
	}

	public int getActiveAmountOfPlayers() {
		return activeAmountOfPlayers;
	}

	public void setActiveAmountOfPlayers(int activeAmountOfPlayers) {
		this.activeAmountOfPlayers = activeAmountOfPlayers;
	}
}
