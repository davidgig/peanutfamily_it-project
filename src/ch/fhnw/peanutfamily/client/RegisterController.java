package ch.fhnw.peanutfamily.client;

import ch.fhnw.peanutfamily.client.communication.ClientGameLogic;
import ch.fhnw.peanutfamily.client.communication.ClientSender;
import ch.fhnw.peanutfamily.common.TransferObject;
import ch.fhnw.peanutfamily.common.UserData;
import ch.fhnw.peanutfamily.common.TransferObject.TransferType;
import ch.fhnw.peanutfamily.resources.templates.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * 
 * @author roger
 *
 */
public class RegisterController extends Controller<RegisterView, RegisterModel> {

	private ClientGameLogic clientGameLogic;

	/**
	 * The RegisterController starts the view and creates EventHandler for all
	 * buttons
	 * 
	 * @param view the View class
	 * @param model the Model class
	 * 
	 * @author Roger
	 */
	public RegisterController(RegisterView view, RegisterModel model) {
		super(view, model);

		view.startUI();

		// Action Handler for Register Button
		view.registerBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				String regName = view.usernameTxt.getText().toString();
				String regPassword = view.passwordTxt.getText().toString();
				
				UserData register = new UserData(regName, regPassword);
				TransferObject transferObject = new TransferObject(TransferType.REGISTER);
				transferObject.setTransferObject(register);
				ClientSender.send(transferObject);
			}
		});

		// Action Handler for Cancel Button
		view.cancelBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				clientGameLogic.createLoginMVC();
			}
		});
	}

	/**
	 * This method shows a registering error message
	 * 
	 * @author Roger
	 */
	public void throwErrorMessage() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Register Error");
		alert.setHeaderText("Username is already existing");
		alert.show();
	}
	
	/**
	 * This method shows a registering success message
	 * 
	 * @author Roger
	 */
	public void throwSuccessMessage() {
		Alert alert1 = new Alert(AlertType.CONFIRMATION);
		alert1.setTitle("Registration Sucessfull");
		alert1.setHeaderText("You are successfull registrated");
		alert1.showAndWait();
	}

	/**
	 * This methods sets the ClientGameLogic to have access to it (for changing pages)
	 * 
	 * @param clientGameLogic the ClientGameLogic
	 * 
	 * @author Roger
	 */
	public void setClientGameLogic(ClientGameLogic clientGameLogic) {

		this.clientGameLogic = clientGameLogic;
	}
}
