package ch.fhnw.peanutfamily.client;

import ch.fhnw.peanutfamily.resources.LanguageTranslator;
import ch.fhnw.peanutfamily.resources.PropertyControl;
import ch.fhnw.peanutfamily.resources.templates.View;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *In this class the Login UI is set up.
 *The menu bar is in every view individual because different options are available
 *It also contains the language update method to update all labels and buttons if a new language is selected.
 *
 * @author roger
 *
 */
public class LoginView extends View<LoginModel> {

	private LanguageTranslator languageTranslator;

	private Scene scene;

	private BorderPane login_pane;

	private VBox loginInfo;

	// Defining Menu Bar
	public MenuBar menuBar;
	public Menu menuFile;
	public Menu menuLanguage;

	private MenuItem menuLanguageDe;
	private MenuItem menuLanguageEn;
	private MenuItem menuClose;

	// Labels
	public Label usernameLbl;
	public Label passwordLbl;
	public Label loginLbl;

	// Textfields
	public TextField usernameTxt;
	public PasswordField passwordTxt;

	// Buttons
	public Button loginBtn;
	public Button newUserBtn;

	public LoginView(Stage stage, LoginModel model) {
		super(stage, model);

		languageTranslator = PropertyControl.getPropertyControl().getLanguageTranslator();
	}

	@Override
	protected void initUI() {

		// Defining Menu Items for menuBar
		menuBar = new MenuBar();
		menuFile = new Menu(languageTranslator.getTranslation("general.menu.menuFile"));
		menuLanguage = new Menu(languageTranslator.getTranslation("general.menu.menuLanguage"));

		menuLanguageDe = new MenuItem(languageTranslator.getTranslation("general.menu.menuLanguageDe"));
		menuLanguageDe.setOnAction(event -> {
			PropertyControl.getPropertyControl().setLanguageTranslator(new LanguageTranslator("DE"));
			updateLanguage();
		});

		menuLanguageEn = new MenuItem(languageTranslator.getTranslation("general.menu.menuLanguageEn"));
		menuLanguageEn.setOnAction(event -> {
			PropertyControl.getPropertyControl().setLanguageTranslator(new LanguageTranslator("EN"));
			updateLanguage();
		});

		menuLanguage.getItems().addAll(menuLanguageDe, menuLanguageEn);

		menuClose = new MenuItem(languageTranslator.getTranslation("general.menu.menuClose"));
		menuClose.setOnAction(event -> {
			stage.close();
		});
		menuFile.getItems().addAll(menuLanguage, menuClose);
		menuBar.getMenus().add(menuFile);
		menuBar.setId("MenuBar");
		menuFile.setId("MenuFile");	
		
		// Defining Login Info on right side
		usernameLbl = new Label(languageTranslator.getTranslation("general.label.usernameLbl"));
		usernameTxt = new TextField();

		passwordLbl = new Label(languageTranslator.getTranslation("general.label.passwordLbl"));
		passwordTxt = new PasswordField();

		loginLbl = new Label(languageTranslator.getTranslation("general.label.loginLbl"));
		
		loginBtn = new Button(languageTranslator.getTranslation("general.buttons.loginBtn"));
		newUserBtn = new Button(languageTranslator.getTranslation("general.buttons.newUserBtn"));

		loginInfo = new VBox();
		loginInfo.getChildren().addAll(usernameLbl, usernameTxt, passwordLbl, passwordTxt, loginBtn, newUserBtn);
	
		// Defining Login Pane (BorderPane).
		login_pane = new BorderPane();
		login_pane.setTop(menuBar);
		login_pane.setRight(loginInfo);
		login_pane.setLeft(loginLbl);

		scene = new Scene(login_pane, 1200, 620);
		setCSS();
	}

	@Override
	public void createUI() {

		stage.setTitle(languageTranslator.getTranslation("general.name"));
		stage.setScene(this.scene);
	}

	/**
	 * This method updates the language of all nodes
	 * 
	 * @author Roger
	 */
	public void updateLanguage() {

		LanguageTranslator manager = PropertyControl.getPropertyControl().getLanguageTranslator();

		// Update Title
		stage.setTitle(manager.getTranslation("general.name"));

		// Update Menu
		menuFile.setText(manager.getTranslation("general.menu.menuFile"));
		menuLanguage.setText(manager.getTranslation("general.menu.menuLanguage"));
		menuLanguageDe.setText(manager.getTranslation("general.menu.menuLanguageDe"));
		menuLanguageEn.setText(manager.getTranslation("general.menu.menuLanguageEn"));
		menuClose.setText(manager.getTranslation("general.menu.menuClose"));

		// Update Labels
		usernameLbl.setText(manager.getTranslation("general.label.usernameLbl"));
		passwordLbl.setText(manager.getTranslation("general.label.passwordLbl"));
		loginLbl.setText(manager.getTranslation("general.label.loginLbl"));

		// Update Buttons
		loginBtn.setText(manager.getTranslation("general.buttons.loginBtn"));
		newUserBtn.setText(manager.getTranslation("general.buttons.newUserBtn"));
	}

	/**
	 * This method desings all nodes
	 */
	private void setCSS() {
	
		//Login Background
		BackgroundImage image = new BackgroundImage(new Image(getClass().getResourceAsStream("variante6.png")),
		        BackgroundRepeat.NO_REPEAT, 
		        BackgroundRepeat.NO_REPEAT, 
		        BackgroundPosition.DEFAULT,
		        BackgroundSize.DEFAULT);		
		login_pane.setBackground(new Background(image));

		//Vbox Position
		loginInfo.setPadding(new Insets(50, 50 , 0, 0));
		loginInfo.setPrefWidth(300);
			
		// loginLbl
		loginLbl.setTextFill(Color.WHITE);
		loginLbl.setFont(new Font("Arial", 70));
		loginLbl.setPadding(new Insets(50, 0, 0, 50));
		
		// usernameLbl
		usernameLbl.setTextFill(Color.WHITE);
		usernameLbl.setPadding(new Insets(0, 0, 0, 0));
		
		// passwordLbl
		passwordLbl.setTextFill(Color.WHITE);
		passwordLbl.setPadding(new Insets(0, 0, 0 , 0));
		
		// loginBtn
		loginBtn.setTextFill(Color.BLACK);
		loginBtn.setPrefWidth(300);
		loginInfo.setMargin(loginBtn, new Insets(50, 0, 5, 0));
		
		// NewUserBtm
		newUserBtn.setPrefWidth(300);
		loginInfo.setMargin(newUserBtn, new Insets(0, 0, 5, 0));
	}
}
