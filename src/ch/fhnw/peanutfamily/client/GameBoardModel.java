package ch.fhnw.peanutfamily.client;

import java.util.ArrayList;
import java.util.List;

import ch.fhnw.peanutfamily.common.Card;
import ch.fhnw.peanutfamily.common.Figure;
import ch.fhnw.peanutfamily.common.Game;
import ch.fhnw.peanutfamily.common.IntermediateResult;
import ch.fhnw.peanutfamily.common.Player;
import ch.fhnw.peanutfamily.common.Tile;
import ch.fhnw.peanutfamily.common.TileStack;
import ch.fhnw.peanutfamily.common.TransferObject.PlayerStatus;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The GameBoard Model holds the ArrayLists for the Tiles, Cards and figueres. (Includes Setter's and getters)
 * @author roger
 *
 */
public class GameBoardModel {

	// attributes
	
	static SimpleStringProperty chatText = new SimpleStringProperty("");

	private Game game = new Game();
	private Player player = new Player();

	private IntermediateResult intermediateResult = null;

	private List<TileStack> tileStackes = new ArrayList<>();
	private List<Card> myCards = new ArrayList<>();
	private List<Tile> myTiles = new ArrayList<>();
	private List<Figure> figures = new ArrayList<>();

	private PlayerStatus playerStatus;

	private String selectedFigure = "";

	private List<String> selectedTiles = new ArrayList<>();
	private List<String> selectedCards = new ArrayList<>();

	// getters/setters
	
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public List<TileStack> getTileStackes() {
		return tileStackes;
	}

	public void setTileStackes(List<TileStack> tileStackes) {
		this.tileStackes = tileStackes;
	}

	public List<Card> getMyCards() {
		return myCards;
	}

	public void setMyCards(List<Card> myCards) {
		this.myCards = myCards;
	}

	public List<Tile> getMyTiles() {
		return myTiles;
	}

	public void setMyTiles(List<Tile> myTiles) {
		this.myTiles = myTiles;
	}

	public List<Figure> getFigures() {
		return figures;
	}

	public void setFigures(List<Figure> figures) {
		this.figures = figures;
	}

	public String getSelectedFigure() {
		return selectedFigure;
	}

	public void setSelectedFigure(String selectedFigure) {
		this.selectedFigure = selectedFigure;
	}

	public PlayerStatus getPlayerStatus() {
		return playerStatus;
	}

	public void setPlayerStatus(PlayerStatus playerStatus) {
		this.playerStatus = playerStatus;
	}

	public IntermediateResult getIntermediateResult() {
		return intermediateResult;
	}

	public void setIntermediateResult(IntermediateResult intermediateResult) {
		this.intermediateResult = intermediateResult;
	}

	public List<String> getSelectedTiles() {
		return selectedTiles;
	}

	public void setSelectedTiles(List<String> selectedTiles) {
		this.selectedTiles = selectedTiles;
	}

	public List<String> getSelectedCards() {
		return selectedCards;
	}

	public void setSelectedCards(List<String> selectedCards) {
		this.selectedCards = selectedCards;
	}

	public String getChatText() {
		return chatText.get();
	}

	public SimpleStringProperty chatTextProperty() {
		return chatText;
	}

	public void setChatText(String authorsName) {
		this.chatText.set(authorsName);
	}
}
