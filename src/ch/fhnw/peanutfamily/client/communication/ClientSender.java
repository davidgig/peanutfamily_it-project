package ch.fhnw.peanutfamily.client.communication;

import java.io.IOException;

import ch.fhnw.peanutfamily.common.TransferObject;

/**
 * The ClientSender sends all actions and informations to the server 
 * 
 * @author David
 *
 */
public class ClientSender {

	/**
	 * This method receives the TransferObject from the the client logic and forwards it to the server 
	 * 
	 * @param transferObject the TransferObject containing a object, type and status
	 */
	public static void send(TransferObject transferObject) {

		try {
			ClientManager.oos.writeObject(transferObject);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
