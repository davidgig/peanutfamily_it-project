package ch.fhnw.peanutfamily.client.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import ch.fhnw.peanutfamily.common.TransferObject;
import ch.fhnw.peanutfamily.resources.SettingControl;
import javafx.stage.Stage;

/**
 * The ClientManager creates a new socket connection to the server and listens for 
 * answers from the server
 * 
 * @author David
 *
 */
public class ClientManager implements Runnable {

	private int port;
	private String ip;
	
	public static ObjectOutputStream oos;
	public static ObjectInputStream ois;
	
	private ClientGameLogic clientGameLogic;
	private Stage primaryStage;
	
	/**
	 * Constructor loaded ip and port from the settings and makes the connection
	 * to the ClientGameLogic
	 * 
	 * @param settingControl the SettingControl
	 * @param primaryStage the stage of the View which is created by JavaFx
	 * 
	 * @author David
	 */
	public ClientManager(SettingControl settingControl, Stage primaryStage) {
		
		this.port = Integer.parseInt(settingControl.getSettings("Port"));
		this.ip = settingControl.getSettings("IP");
		
		this.clientGameLogic = new ClientGameLogic(primaryStage);
	}

	/**
	 * This function creates actually the input and output stream to the server
	 * and send all data from the server to the clien logic
	 * 
	 * @author David
	 */
	@Override
	public void run() {

		try {
			Socket socket = new Socket(ip, port);
 
			this.oos = new ObjectOutputStream(socket.getOutputStream());
			this.ois = new ObjectInputStream(socket.getInputStream());

			while (true) {
				TransferObject transferObject = (TransferObject) ois.readObject();
				clientGameLogic.executeEvent(transferObject);
			}
		} catch (UnknownHostException e) {
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
		}
	}
}
