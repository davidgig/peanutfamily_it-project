package ch.fhnw.peanutfamily.client.communication;

import java.util.logging.Logger;

import ch.fhnw.peanutfamily.client.GameBoardController;
import ch.fhnw.peanutfamily.client.GameBoardModel;
import ch.fhnw.peanutfamily.client.GameBoardView;
import ch.fhnw.peanutfamily.client.LobbyController;
import ch.fhnw.peanutfamily.client.LobbyModel;
import ch.fhnw.peanutfamily.client.LobbyView;
import ch.fhnw.peanutfamily.client.LoginController;
import ch.fhnw.peanutfamily.client.LoginModel;
import ch.fhnw.peanutfamily.client.LoginView;
import ch.fhnw.peanutfamily.client.RegisterController;
import ch.fhnw.peanutfamily.client.RegisterModel;
import ch.fhnw.peanutfamily.client.RegisterView;
import ch.fhnw.peanutfamily.common.Game;
import ch.fhnw.peanutfamily.common.GameBoard;
import ch.fhnw.peanutfamily.common.GameResult;
import ch.fhnw.peanutfamily.common.IntermediateResult;
import ch.fhnw.peanutfamily.common.LoginResult;
import ch.fhnw.peanutfamily.common.TransferObject;
import ch.fhnw.peanutfamily.resources.PropertyControl;
import javafx.application.Platform;
import javafx.stage.Stage;

/**
 * The ClientGameLogic executes all the actions which are send from the server
 * 
 * @author David
 *
 */
public class ClientGameLogic {

	private Logger logger = null;
	private Stage primaryStage = null;

	private LoginController loginController = null;
	private RegisterController registerController = null;
	private LobbyController lobbyController = null;
	private GameBoardController gameBoardController = null;

	/**
	 * Constructor creating the the logger and setting the stage
	 * 
	 * @author David
	 */
	public ClientGameLogic(Stage primaryStage) {

		logger = PropertyControl.getPropertyControl().getLoggerControl().getLogger();
		this.primaryStage = primaryStage;
	}

	/**
	 * This method executes the actions of the transferObject calling the
	 * corresponding controller of the view and creates them if not already
	 * existing
	 * 
	 * @param transferObject
	 */
	public void executeEvent(TransferObject transferObject) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {

				switch (transferObject.getTransferType()) {
				case CONNECTION_ESTABLISHED:
					createLoginMVC();
					break;
				case LOGIN_SUCCESS:
					createLobbyMVC();
					lobbyController.setLoginResult((LoginResult) transferObject.getTransferObject());
					break;
				case LOGIN_FAIL:
					loginController.throwErrorMessage();
					break;
				case REGISTER_SUCCESS:
					registerController.throwSuccessMessage();
					createLoginMVC();
					break;
				case REGISTER_FAIL:
					registerController.throwErrorMessage();
					break;
				case NEWCREATEDGAME:
					if (lobbyController != null) {
						lobbyController.addNewCreatedGame((Game) transferObject.getTransferObject());
					}
					break;
				case INITGAMEBOARD:
					if (gameBoardController == null) {
						createGameBoardMVC();
					}
					gameBoardController.initGameBoard((GameBoard) transferObject.getTransferObject(),
							transferObject.getPlayerStatus());
					break;
				case MAKEAMOVERESULT:
					if (gameBoardController != null) {
						gameBoardController.initGameBoard((GameBoard) transferObject.getTransferObject(),
								transferObject.getPlayerStatus());
					}
					break;
				case MESSAGE:
					if (gameBoardController != null) {
						gameBoardController.showMessage((String) transferObject.getTransferObject());
					}
					break;
				case PLAYERACTION:
					if (gameBoardController != null) {
						gameBoardController.doPlayerStatus(transferObject.getPlayerStatus());
					}
					break;
				case INTERMEDIATERESULT:
					if (gameBoardController != null) {
						gameBoardController.initIntermediateResult(
								(IntermediateResult) transferObject.getTransferObject(),
								transferObject.getPlayerStatus());
					}
					break;
				case FINISHED:
					if (gameBoardController != null) {
						gameBoardController.finishGame((GameResult) transferObject.getTransferObject(),
								transferObject.getPlayerStatus());
					}
				default:
				}
			}
		});
	}

	/**
	 * This method creates the Model View Controller of the login page
	 */
	public void createLoginMVC() {

		LoginModel loginModel = new LoginModel();
		logger.config("LoginModel created");

		LoginView loginView = new LoginView(primaryStage, loginModel);
		logger.config("LoginView created");

		loginController = new LoginController(loginView, loginModel);
		loginController.setClientGameLogic(this);
		logger.config("LoginController created");
	}

	/**
	 * This method creates the Model View Controller of the register page
	 */
	public void createRegisterMVC() {

		RegisterModel registerModel = new RegisterModel();
		logger.config("RegisterModel created");

		RegisterView registerView = new RegisterView(primaryStage, registerModel);
		logger.config("RegisterView created");

		registerController = new RegisterController(registerView, registerModel);
		registerController.setClientGameLogic(this);
		logger.config("RegisterController created");
	}

	/**
	 * This method creates the Model View Controller of the lobby page
	 */
	public void createLobbyMVC() {

		LobbyModel lobbyModel = new LobbyModel();
		logger.config("LobbyModel created");

		LobbyView lobbyView = new LobbyView(primaryStage, lobbyModel);
		logger.config("Lobby View created");

		lobbyController = new LobbyController(lobbyView, lobbyModel);
		lobbyController.setClientGameLogic(this);
		logger.config("Lobby Controller created");

	}

	/**
	 * This method creates the Model View Controller of the GameBoard
	 */
	public void createGameBoardMVC() {

		GameBoardModel gameBoardModel = new GameBoardModel();
		logger.config("GameBoardModel created");

		GameBoardView gameBoardView = new GameBoardView(primaryStage, gameBoardModel);
		logger.config("GameBoard View created");

		gameBoardController = new GameBoardController(gameBoardView, gameBoardModel);
		logger.config("GameBoard Controller created");
	}
}
