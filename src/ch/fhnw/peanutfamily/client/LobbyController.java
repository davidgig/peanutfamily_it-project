package ch.fhnw.peanutfamily.client;

import ch.fhnw.peanutfamily.client.communication.ClientGameLogic;
import ch.fhnw.peanutfamily.client.communication.ClientSender;
import ch.fhnw.peanutfamily.common.CreateGame;
import ch.fhnw.peanutfamily.common.Game;
import ch.fhnw.peanutfamily.common.JoinGame;
import ch.fhnw.peanutfamily.common.LoginResult;
import ch.fhnw.peanutfamily.common.TransferObject;
import ch.fhnw.peanutfamily.common.TransferObject.TransferType;
import ch.fhnw.peanutfamily.resources.templates.Controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.Alert.AlertType;


public class LobbyController extends Controller<LobbyView, LobbyModel> {

	private ClientGameLogic clientGameLogic;
	
	/**
	 * The LobbyController starts the view and creates EventHandler for all
	 * buttons and comboboxes.
	 * 
	 * @param view the View class
	 * @param model the Model class
	 * 
	 * @author Roger
	 */
	public LobbyController(LobbyView view, LobbyModel model) {
		super(view, model);

		view.startUI();

		/*
		 * logout from the lobby and redirect to the login page
		 */
		view.menuLogOut.setOnAction(event -> {
			clientGameLogic.createLoginMVC();
		});

		/*
		 * select a number of players 
		 */
		view.group1.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			public void changed(ObservableValue<? extends Toggle> ov, Toggle toggle, Toggle new_toggle) {

				RadioButton radioBtn = (RadioButton) new_toggle.getToggleGroup().getSelectedToggle();
				model.setActiveAmountOfPlayers(Integer.parseInt(radioBtn.getText()));
			}
		});

		/*
		 * After choosing an amount of players and a name it send the info to the server
		 * to create a new game
		 */
		view.startNewBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				if (view.gameNameTxt.getText().equals("")) {
					throwErrorMessage("Please enter a new game name!");
				} else {

					CreateGame createGame = new CreateGame();
					createGame.setGameName(view.gameNameTxt.getText());
					createGame.setAmountOfPlayers(model.getActiveAmountOfPlayers());
					createGame.setMyUser(model.getMyUser());

					TransferObject transferObject = new TransferObject(TransferType.CREATEGAME);
					transferObject.setTransferObject(createGame);

					ClientSender.send(transferObject);
				}
			}
		});

		/*
		 * After selecting one of the existing games, it informs the server that this
		 * user wants to join it 
		 */
		view.joinGameBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				if (model.getActiveJoinGameId().equals("")) {
					throwErrorMessage("Please select a game to join!");
				} else {

					JoinGame joinGame = new JoinGame();
					joinGame.setGameId(model.getActiveJoinGameId());
					joinGame.setMyUser(model.getMyUser());

					TransferObject transferObject = new TransferObject(TransferType.JOINGAME);
					transferObject.setTransferObject(joinGame);

					ClientSender.send(transferObject);
				}
			}
		});

		/*
		 * Selects the the chosen game from the list of exisiting games
		 */
		view.joinGameBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Game>() {
			@Override
			public void changed(ObservableValue<? extends Game> observable, Game oldValue, Game newValue) {

				model.setActiveJoinGameId(newValue.getId());
			}
		});
	}

	/**
	 * This methods sets the ClientGameLogic to have access to it (for changing pages)
	 * 
	 * @param clientGameLogic the ClientGameLogic
	 * 
	 * @author Roger
	 */
	public void setClientGameLogic(ClientGameLogic clientGameLogic) {

		this.clientGameLogic = clientGameLogic;
	}

	/**
	 * After the user is logged in his information and all open games are be placed on the page
	 * 
	 * @param loginResult the result containing player info and all games
	 * 
	 * @author Roger
	 */
	public void setLoginResult(LoginResult loginResult) {

		model.setMyUser(loginResult.getUser());

		ObservableList<Game> savedGamges = FXCollections.observableArrayList(loginResult.getSavedGames());
		model.setNewGames(savedGamges);

		ObservableList<Game> newGames = FXCollections.observableArrayList(loginResult.getNewGames());
		model.setNewGames(newGames);

		view.setData();
	}

	/**
	 * Thsi method shows an error popup
	 * 
	 * @param message the erro text to show 
	 * 
	 * @author Roger
	 */
	public void throwErrorMessage(String message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(message);
		alert.show();
	}

	/**
	 * Adds a new created game to the list of existing games
	 * 
	 * @param newGame a Game which was new created
	 * 
	 * @author Roger
	 */
	public void addNewCreatedGame(Game newGame) {

		model.getNewGames().add(newGame);
		view.setData();
	}
}
