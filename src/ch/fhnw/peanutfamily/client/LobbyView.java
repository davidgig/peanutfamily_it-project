package ch.fhnw.peanutfamily.client;

import java.util.List;

import ch.fhnw.peanutfamily.common.Game;
import ch.fhnw.peanutfamily.common.User;
import ch.fhnw.peanutfamily.resources.LanguageTranslator;
import ch.fhnw.peanutfamily.resources.PropertyControl;
import ch.fhnw.peanutfamily.resources.templates.View;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * 
 * @author roger
 *
 */
public class LobbyView extends View<LobbyModel> {

	private LanguageTranslator languageTranslator;

	private Scene scene;

	private BorderPane lobby_pane;

	private BorderPane upperBox;
	private BorderPane lowerBox;

	private VBox gameJoin;
	private VBox gameLoad;
	private GridPane gameInfo;
	private GridPane gameSetUp;

	// Defining MenuBar
	public MenuBar menuBar;
	public Menu menuFile;
	public Menu menuLanguage;

	public MenuItem menuLanguageDe;
	public MenuItem menuLanguageEn;
	public MenuItem menuClose;
	public MenuItem menuLogOut;

	// General Label
	public Label lobbyLbl;

	// Labels top Left
	public Label gameNameLbl;
	public Label nrPlayerLbl;
	public Label languageLbl;

	// Labels top right
	public Label usernameLbl;
	public Label winsLbl;
	public Label highscoreLbl;
	public Label playingUserLbl;
	public Label playingUserPointsLbl;
	public Label playingUserHighscoreLbl;

	// Labels bottom left
	public Label joinGameLbl;

	// Textfields
	public TextField gameNameTxt;

	// Buttons top Left Grip Pane
	public Button startNewBtn;
	public Button logOutBtn;
	public ToggleGroup group1;
	public ToggleGroup group2;
	public RadioButton nr1PlayerBtn;
	public RadioButton nr2PlayerBtn;
	public RadioButton nr3PlayerBtn;
	public RadioButton deBtn;
	public RadioButton enBtn;

	// Buttons bottom BorderPane
	public Button joinGameBtn;

	// Dropdown selections
	public ComboBox<Game> joinGameBox;

	public HBox nrPlayerBox;
	public HBox languageSelectBox;

	public LobbyView(Stage stage, LobbyModel model) {
		super(stage, model);

		languageTranslator = PropertyControl.getPropertyControl().getLanguageTranslator();
	}

	@Override
	protected void initUI() {

		// Defining Menu Items for menuBar
		menuBar = new MenuBar();
		menuFile = new Menu(languageTranslator.getTranslation("general.menu.menuFile"));
		menuLanguage = new Menu(languageTranslator.getTranslation("general.menu.menuLanguage"));

		menuLanguageDe = new MenuItem(languageTranslator.getTranslation("general.menu.menuLanguageDe"));
		menuLanguageDe.setOnAction(event -> {
			PropertyControl.getPropertyControl().setLanguageTranslator(new LanguageTranslator("DE"));
			updateLanguage();
		});

		menuLanguageEn = new MenuItem(languageTranslator.getTranslation("general.menu.menuLanguageEn"));
		menuLanguageEn.setOnAction(event -> {
			PropertyControl.getPropertyControl().setLanguageTranslator(new LanguageTranslator("EN"));
			updateLanguage();
		});

		menuLanguage.getItems().addAll(menuLanguageDe, menuLanguageEn);

		menuClose = new MenuItem(languageTranslator.getTranslation("general.menu.menuClose"));
		menuClose.setOnAction(event -> {
			stage.close();
		});

		menuLogOut = new MenuItem(languageTranslator.getTranslation("general.menu.menuLogOut"));

		menuFile.getItems().addAll(menuLanguage, menuLogOut, menuClose);
		menuBar.getMenus().add(menuFile);

		// TOP LEFT ZONE
		// Defining GameSetUp Pane visible on the top left
		gameNameLbl = new Label(languageTranslator.getTranslation("lobby.label.gameNameLbl"));
		startNewBtn = new Button(languageTranslator.getTranslation("lobby.buttons.startNewBtn"));
		logOutBtn = new Button(languageTranslator.getTranslation("lobby.buttons.logOutBtn"));
		nrPlayerLbl = new Label(languageTranslator.getTranslation("lobby.label.nrPlayerLbl"));
		languageLbl = new Label(languageTranslator.getTranslation("lobby.label.languageLbl"));
		lobbyLbl = new Label(languageTranslator.getTranslation("lobby.label.lobbyLbl"));
		gameSetUp = new GridPane();
		gameNameTxt = new TextField();

		// Defining the radio buttons for Nr. of Players
		nrPlayerBox = new HBox();
		group1 = new ToggleGroup();
		RadioButton nr1PlayerBtn = new RadioButton("2");
		nr1PlayerBtn.setToggleGroup(group1);
		nr1PlayerBtn.setSelected(true);
		RadioButton nr2PlayerBtn = new RadioButton("3");
		nr2PlayerBtn.setToggleGroup(group1);
		RadioButton nr3PlayerBtn = new RadioButton("4");
		nr3PlayerBtn.setToggleGroup(group1);
		nrPlayerBox.getChildren().addAll(nr1PlayerBtn, nr2PlayerBtn, nr3PlayerBtn);

		// Filling up the Gripd Pane top Left
		gameSetUp.add(nrPlayerLbl, 0, 0);
		gameSetUp.add(nrPlayerBox, 1, 0);
		gameSetUp.add(gameNameLbl, 0, 1);
		gameSetUp.add(gameNameTxt, 1, 1);
		gameSetUp.add(startNewBtn, 1, 2);

		// TOP RIGHT ZONE
		// Defining GameInfo Pane visible on the top right
		usernameLbl = new Label(languageTranslator.getTranslation("general.label.usernameLbl"));
		winsLbl = new Label(languageTranslator.getTranslation("lobby.label.winsLbl"));
		highscoreLbl = new Label(languageTranslator.getTranslation("lobby.label.highscoreLbl"));
		playingUserLbl = new Label();
		playingUserHighscoreLbl = new Label();
		playingUserPointsLbl = new Label();

		gameInfo = new GridPane();

		// Filling up the GridPane top Right
		gameInfo.add(usernameLbl, 0, 0);
		gameInfo.add(winsLbl, 0, 1);
		gameInfo.add(highscoreLbl, 0, 2);
		gameInfo.add(playingUserLbl, 1, 0);
		gameInfo.add(playingUserPointsLbl, 1, 1);
		gameInfo.add(playingUserHighscoreLbl, 1, 2);

		// Defining Game Loading Box visible on the bottom left
		joinGameLbl = new Label(languageTranslator.getTranslation("lobby.label.joinGameLbl"));
		joinGameBox = new ComboBox<>();
		joinGameBox.setCellFactory(new Callback<ListView<Game>, ListCell<Game>>() {
			@Override
			public ListCell<Game> call(ListView<Game> p) {
				ListCell cell = new ListCell<Game>() {
					@Override
					protected void updateItem(Game item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setText("");
						} else {
							setText(item.getName());
						}
					}
				};
				return cell;
			}
		});

		joinGameBtn = new Button(languageTranslator.getTranslation("lobby.buttons.joinGameBtn"));
		gameJoin = new VBox();
		gameJoin.getChildren().addAll(joinGameLbl, joinGameBox, joinGameBtn);

		// Adding Boxes/panes to BorderPane
		upperBox = new BorderPane();
		upperBox.setLeft(gameSetUp);
		upperBox.setRight(gameInfo);
		upperBox.setTop(lobbyLbl);

		lowerBox = new BorderPane();
		lowerBox.setLeft(gameJoin);
		lowerBox.setRight(gameLoad);

		lobby_pane = new BorderPane();
		lobby_pane.setTop(menuBar);
		lobby_pane.setCenter(upperBox);
		lobby_pane.setBottom(lowerBox);

		scene = new Scene(lobby_pane, 1200, 620);

		setCSS();
		setData();
	}

	@Override
	public void createUI() {

		stage.setTitle(languageTranslator.getTranslation("general.name"));
		stage.setScene(this.scene);
	}

	/**
	 * This method updates the language of all nodes
	 * 
	 * @author Roger
	 */
	public void updateLanguage() {

		LanguageTranslator manager = PropertyControl.getPropertyControl().getLanguageTranslator();

		// Update Title
		stage.setTitle(manager.getTranslation("general.name"));

		// Update Menu
		menuFile.setText(manager.getTranslation("general.menu.menuFile"));
		menuLanguage.setText(manager.getTranslation("general.menu.menuLanguage"));
		menuLanguageDe.setText(manager.getTranslation("general.menu.menuLanguageDe"));
		menuLanguageEn.setText(manager.getTranslation("general.menu.menuLanguageEn"));
		menuLogOut.setText(manager.getTranslation("general.menu.menuLogOut"));
		menuClose.setText(manager.getTranslation("general.menu.menuClose"));

		// Update Labels
		gameNameLbl.setText(manager.getTranslation("lobby.label.gameNameLbl"));
		nrPlayerLbl.setText(manager.getTranslation("lobby.label.nrPlayerLbl"));
		languageLbl.setText(manager.getTranslation("lobby.label.languageLbl"));
		usernameLbl.setText(manager.getTranslation("general.label.usernameLbl"));
		joinGameLbl.setText(manager.getTranslation("lobby.label.joinGameLbl"));
		lobbyLbl.setText(manager.getTranslation("lobby.label.lobbyLbl"));
		winsLbl.setText(manager.getTranslation("lobby.label.winsLbl"));
		highscoreLbl.setText(manager.getTranslation("lobby.label.highscoreLbl"));

		// Update Buttons
		startNewBtn.setText(manager.getTranslation("lobby.buttons.startNewBtn"));
		logOutBtn.setText(manager.getTranslation("lobby.buttons.logOutBtn"));
		joinGameBtn.setText(manager.getTranslation("lobby.buttons.joinGameBtn"));
	}

	/**
	 * This method designes all the nodes
	 * 
	 * @author Roger
	 */
	public void setCSS() {

		// Login Background
		BackgroundImage image = new BackgroundImage(new Image(getClass().getResourceAsStream("variante6.png")),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);
		lobby_pane.setBackground(new Background(image));

		// Labels top left
		// Game Name Label
		gameNameLbl.setTextFill(Color.WHITE);

		// Number of Player Label
		nrPlayerLbl.setTextFill(Color.WHITE);

		// language Label
		languageLbl.setTextFill(Color.WHITE);

		// Labels TOP right
		// Wins Label
		winsLbl.setTextFill(Color.WHITE);

		// Highscore Label
		highscoreLbl.setTextFill(Color.WHITE);

		// Username Label
		usernameLbl.setTextFill(Color.WHITE);

		// Player Username
		playingUserLbl.setTextFill(Color.RED);

		// Player nr of Wins Label
		playingUserPointsLbl.setTextFill(Color.RED);

		// Player highscore Label
		playingUserHighscoreLbl.setTextFill(Color.RED);

		// General Label
		// Lobby Label
		lobbyLbl.setTextFill(Color.WHITE);
		lobbyLbl.setFont(new Font("Arial", 70));

		// Bottom Labels
		// join Game Label
		joinGameLbl.setTextFill(Color.WHITE);

		// Upper Box
		upperBox.setPadding(new Insets(50, 50, 50, 50));
		upperBox.setPrefWidth(800);

		// Upper Left Side
		gameSetUp.setPadding(new Insets(60, 0, 0, 0));
		gameSetUp.setPrefWidth(600);
		gameSetUp.setHgap(30);
		gameSetUp.setVgap(10);

		// Upper Right Side
		gameInfo.setPadding(new Insets(50, 0, 0, 0));
		gameInfo.setPrefWidth(200);
		gameInfo.setHgap(20);
		gameInfo.setVgap(5);

		// Upper Box Buttons
		startNewBtn.setPrefWidth(250);
		startNewBtn.setPrefHeight(20);

		// toggle group
		nrPlayerBox.setSpacing(10);

		// Lower Box
		lowerBox.setPadding(new Insets(50, 50, 50, 50));
		lowerBox.setPrefWidth(800);
		lowerBox.setPrefHeight(200);

		// Join Game Box
		joinGameBtn.setPrefWidth(250);
		joinGameBtn.setPrefHeight(20);
		joinGameBox.setPrefWidth(250);
		joinGameBox.setPrefHeight(20);
		gameJoin.setSpacing(10);
	}

	/**
	 * This method set the data for the player info and the game list
	 * 
	 * @author Roger
	 */
	public void setData() {

		playingUserLbl.setText(model.getMyUser().getName());
		playingUserHighscoreLbl.setText("" + model.getMyUser().getHighscore());
		playingUserPointsLbl.setText("" + model.getMyUser().getVictories());

		joinGameBox.setItems(model.getNewGames());
	}
}