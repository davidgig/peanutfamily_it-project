package ch.fhnw.peanutfamily.client;

import ch.fhnw.peanutfamily.resources.LanguageTranslator;
import ch.fhnw.peanutfamily.resources.PropertyControl;
import ch.fhnw.peanutfamily.resources.templates.View;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class RegisterView extends View<RegisterModel> {

	private LanguageTranslator languageTranslator;

	// Layout
	public Scene scene;
	public BorderPane register_pane;
	public VBox registerInfo;

	// Defining Menu Bar
	public MenuBar menuBar;
	public Menu menuFile;
	public Menu menuLanguage;

	private MenuItem menuLanguageDe;
	private MenuItem menuLanguageEn;
	private MenuItem menuClose;

	// Labels
	public Label usernameLbl;
	public Label passwordLbl;
	public Label loginLbl;
	public Label registerLbl;

	// Textfields
	public TextField usernameTxt;
	public PasswordField passwordTxt;

	// Buttons
	public Button registerBtn;
	public Button cancelBtn;

	public RegisterView(Stage stage, RegisterModel model) {
		super(stage, model);

		languageTranslator = PropertyControl.getPropertyControl().getLanguageTranslator();
	}

	@Override
	protected void initUI() {

		// Defining Menu Items for menuBar
		menuBar = new MenuBar();
		menuFile = new Menu(languageTranslator.getTranslation("general.menu.menuFile"));
		menuLanguage = new Menu(languageTranslator.getTranslation("general.menu.menuLanguage"));

		menuLanguageDe = new MenuItem(languageTranslator.getTranslation("general.menu.menuLanguageDe"));
		menuLanguageDe.setOnAction(event -> {
			PropertyControl.getPropertyControl().setLanguageTranslator(new LanguageTranslator("DE"));
			updateLanguage();
		});

		menuLanguageEn = new MenuItem(languageTranslator.getTranslation("general.menu.menuLanguageEn"));
		menuLanguageEn.setOnAction(event -> {
			PropertyControl.getPropertyControl().setLanguageTranslator(new LanguageTranslator("EN"));
			updateLanguage();
		});

		menuLanguage.getItems().addAll(menuLanguageDe, menuLanguageEn);

		menuClose = new MenuItem(languageTranslator.getTranslation("general.menu.menuClose"));
		menuClose.setOnAction(event -> {
			stage.close();
		});
		menuFile.getItems().addAll(menuLanguage, menuClose);
		menuBar.getMenus().add(menuFile);

		// Defining Registration Info on the right
		usernameLbl = new Label(languageTranslator.getTranslation("general.label.usernameLbl"));
		usernameTxt = new TextField();
		registerLbl = new Label(languageTranslator.getTranslation("general.label.registerLbl"));

		passwordLbl = new Label(languageTranslator.getTranslation("general.label.passwordLbl"));
		passwordLbl.getStyleClass().add("menuLabels");
		passwordTxt = new PasswordField();

		registerBtn = new Button(languageTranslator.getTranslation("general.buttons.registerBtn"));
		cancelBtn = new Button(languageTranslator.getTranslation("general.buttons.cancelBtn"));

		registerInfo = new VBox();
		registerInfo.getChildren().addAll(usernameLbl, usernameTxt, passwordLbl, passwordTxt, registerBtn, cancelBtn);
		registerInfo.getStyleClass().add("vbox");

		register_pane = new BorderPane();
		register_pane.setTop(menuBar);
		register_pane.setLeft(registerLbl);
		register_pane.setRight(registerInfo);

		scene = new Scene(register_pane, 1200, 620);

		setCSS();

	}

	@Override
	protected void createUI() {

		stage.setTitle(languageTranslator.getTranslation("general.name"));
		stage.setScene(this.scene);
	}

	/**
	 * This method updates the language of all nodes
	 * 
	 * @author Roger
	 */
	public void updateLanguage() {

		LanguageTranslator manager = PropertyControl.getPropertyControl().getLanguageTranslator();

		// Update Title
		stage.setTitle(manager.getTranslation("general.name"));

		// Update Menu
		menuFile.setText(manager.getTranslation("general.menu.menuFile"));
		menuLanguage.setText(manager.getTranslation("general.menu.menuLanguage"));
		menuLanguageDe.setText(manager.getTranslation("general.menu.menuLanguageDe"));
		menuLanguageEn.setText(manager.getTranslation("general.menu.menuLanguageEn"));
		menuClose.setText(manager.getTranslation("general.menu.menuClose"));

		// Update Labels
		usernameLbl.setText(manager.getTranslation("general.label.usernameLbl"));
		passwordLbl.setText(manager.getTranslation("general.label.passwordLbl"));
		registerLbl.setText(manager.getTranslation("general.label.registerLbl"));

		// Update Buttons
		registerBtn.setText(manager.getTranslation("general.buttons.registerBtn"));
		cancelBtn.setText(manager.getTranslation("general.buttons.cancelBtn"));

	}

	/**
	 * This method sets the design of all nodes.
	 * 
	 */
	private void setCSS() {

		// Login Background
		BackgroundImage image = new BackgroundImage(new Image(getClass().getResourceAsStream("variante6.png")),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);
		register_pane.setBackground(new Background(image));

		// Vbox Position
		registerInfo.setPadding(new Insets(50, 50, 0, 0));
		registerInfo.setPrefWidth(300);

		// registerLbl
		registerLbl.setTextFill(Color.WHITE);
		registerLbl.setFont(new Font("Arial", 70));
		registerLbl.setPadding(new Insets(50, 0, 0, 50));

		// usernameLbl
		usernameLbl.setTextFill(Color.WHITE);
		usernameLbl.setPadding(new Insets(0, 0, 0, 0));

		// passwordLbl
		passwordLbl.setTextFill(Color.WHITE);
		passwordLbl.setPadding(new Insets(0, 0, 0, 0));

		// registerBtn
		registerBtn.setTextFill(Color.BLACK);
		registerBtn.setPrefWidth(300);
		registerInfo.setMargin(registerBtn, new Insets(50, 0, 5, 0));

		// NewUserBtm
		cancelBtn.setPrefWidth(300);
		registerInfo.setMargin(cancelBtn, new Insets(0, 0, 5, 0));
	}
}
