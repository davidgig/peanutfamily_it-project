package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.UUID;

public class Player implements Serializable{

	private static final long serialVersionUID = 1007355227880138191L;

	// attributes
	
	private String id;
	private String color;
	private String fkUserId;
	private String fkGameId;

	// constructors
	
	public Player() {
		this.id = UUID.randomUUID().toString();
	}

	public Player(String color, String fkUserId, String fkGameId) {
		this.id = UUID.randomUUID().toString();
		this.color = color;
		this.fkUserId = fkUserId;
		this.fkGameId = fkGameId;
	}

	// getters/setters
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getFkUserId() {
		return fkUserId;
	}

	public void setFkUserId(String fkUserId) {
		this.fkUserId = fkUserId;
	}

	public String getFkGameId() {
		return fkGameId;
	}

	public void setFkGameId(String fkGameId) {
		this.fkGameId = fkGameId;
	}
}
