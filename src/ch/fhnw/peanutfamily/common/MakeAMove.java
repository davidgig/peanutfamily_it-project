package ch.fhnw.peanutfamily.common;

import java.io.Serializable;

public class MakeAMove implements Serializable{
	
	private static final long serialVersionUID = -3800561821200919745L;
	
	// attributes
	
	private String gameId;
	private String playerId;
	private String selectedFigure;
	private String selectedCard;
	
	// constructors
	
	public MakeAMove() {
		
	}

	// getters/setters
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getSelectedFigure() {
		return selectedFigure;
	}

	public void setSelectedFigure(String selectedFigure) {
		this.selectedFigure = selectedFigure;
	}

	public String getSelectedCard() {
		return selectedCard;
	}

	public void setSelectedCard(String selectedCard) {
		this.selectedCard = selectedCard;
	}
}
