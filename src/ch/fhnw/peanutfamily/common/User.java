package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.UUID;

public class User implements Serializable {

	private static final long serialVersionUID = 8289451151070898650L;

	// attributes

	private String id;
	private String name;
	private String password;
	private int highscore;
	private int victories;

	// constructors

	public User() {
		this.id = UUID.randomUUID().toString();
	}

	public User(String name, String password, int highscore, int victories) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.password = password;
		this.highscore = highscore;
		this.victories = victories;
	}

	// getters/setters

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getHighscore() {
		return highscore;
	}

	public void setHighscore(int highscore) {
		this.highscore = highscore;
	}

	public int getVictories() {
		return victories;
	}

	public void setVictories(int victories) {
		this.victories = victories;
	}
}
