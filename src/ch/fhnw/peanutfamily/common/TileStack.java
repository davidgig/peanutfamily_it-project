package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TileStack implements Serializable {

	private static final long serialVersionUID = -4960294385376277915L;

	// attributes
	
	private String id;
	private int position;
	private List<Tile> tiles;
	private List<Integer> playerIds;
	private boolean hasBridge;
	private boolean isAtlantis;
	private boolean isLand;
	private boolean isWater;
	private String fkGameId;

	// constructors
	
	public TileStack() {
		this.id = UUID.randomUUID().toString();
		tiles = new ArrayList<>();
		playerIds = new ArrayList<>();
		hasBridge = false;
		isAtlantis = false;
		isLand = false;
		isWater = false;
	}

	public TileStack(int position, List<Tile> tiles, List<Integer> playerIds, boolean hasBridge) {
		this.id = UUID.randomUUID().toString();
		this.position = position;
		this.tiles = tiles;
		this.playerIds = playerIds;
		this.hasBridge = hasBridge;
		isAtlantis = false;
		isLand = false;
		isWater = false;
	}

	// getters/setters
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public List<Tile> getTiles() {
		return tiles;
	}

	public void setTiles(List<Tile> tiles) {
		this.tiles = tiles;
	}

	public List<Integer> getPlayerIds() {
		return playerIds;
	}

	public void setPlayerIds(List<Integer> playerIds) {
		this.playerIds = playerIds;
	}

	public boolean isHasBridge() {
		return hasBridge;
	}

	public void setHasBridge(boolean hasBridge) {
		this.hasBridge = hasBridge;
	}

	public boolean isAtlantis() {
		return isAtlantis;
	}

	public void setAtlantis(boolean isAtlantis) {
		this.isAtlantis = isAtlantis;
	}

	public boolean isLand() {
		return isLand;
	}

	public void setLand(boolean isLand) {
		this.isLand = isLand;
	}

	public boolean isWater() {
		return isWater;
	}

	public void setWater(boolean isWater) {
		this.isWater = isWater;
	}

	public String getFkGameId() {
		return fkGameId;
	}

	public void setFkGameId(String fkGameId) {
		this.fkGameId = fkGameId;
	}
}
