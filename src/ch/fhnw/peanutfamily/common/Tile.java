package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.UUID;

public class Tile implements Serializable {

	private static final long serialVersionUID = 7636431614834949851L;

	// attributes

	private String id;
	private TileNumber tileNumber;
	private TileColor tileColor;
	private String fkTileStackId;
	private String fkPlayerId;

	// constructors

	public Tile() {
		this.id = UUID.randomUUID().toString();
	}

	public Tile(TileNumber tileNumber, TileColor tileColor, String fkTileStackId) {
		this.id = UUID.randomUUID().toString();
		this.tileNumber = tileNumber;
		this.tileColor = tileColor;
		this.fkTileStackId = fkTileStackId;
	}

	// getters/setters

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TileNumber getTileNumber() {
		return tileNumber;
	}

	public void setTileNumber(TileNumber tileNumber) {
		this.tileNumber = tileNumber;
	}

	public TileColor getTileColor() {
		return tileColor;
	}

	public void setTileColor(TileColor tileColor) {
		this.tileColor = tileColor;
	}

	public String getFkTileStackId() {
		return fkTileStackId;
	}

	public void setFkTileStackId(String fkTileStackId) {
		this.fkTileStackId = fkTileStackId;
	}

	public String getFkPlayerId() {
		return fkPlayerId;
	}

	public void setFkPlayerId(String fkPlayerId) {
		this.fkPlayerId = fkPlayerId;
	}

	/*
	 * Tiles can have a number between 1 and 7
	 */
	public static enum TileNumber {
		ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7);

		private final int value;

		private TileNumber(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	/*
	 * Tiles can have one of 7 different colours
	 */
	public static enum TileColor {
		BLUE, BROWN, GREEN, GREY, PINK, WHITE, YELLOW
	}
}
