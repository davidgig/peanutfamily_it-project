package ch.fhnw.peanutfamily.common;

import java.io.Serializable;

public class UserData implements Serializable{

	private static final long serialVersionUID = -8820738981851713866L;
	
	// attributes
	
	private String name;
	private String password;

	// constructors
	
	public UserData(String name, String password) {
		this.name = name;
		this.password = password;
	}

	// getters/setters
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
