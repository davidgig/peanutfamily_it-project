package ch.fhnw.peanutfamily.common;

import java.io.Serializable;

public class CreateGame implements Serializable {

	private static final long serialVersionUID = -4824756628274371026L;

	// attributes
	
	private User myUser;

	private String gameName;
	private int amountOfPlayers;

	// constructors
	
	public CreateGame() {

	}

	public CreateGame(User myUser, String gameName, int amountOfPlayers) {
		this.myUser = myUser;
		this.gameName = gameName;
		this.amountOfPlayers = amountOfPlayers;
	}

	// getters/setters
	
	public User getMyUser() {
		return myUser;
	}

	public void setMyUser(User myUser) {
		this.myUser = myUser;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public int getAmountOfPlayers() {
		return amountOfPlayers;
	}

	public void setAmountOfPlayers(int amountOfPlayers) {
		this.amountOfPlayers = amountOfPlayers;
	}
}