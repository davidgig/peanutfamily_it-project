package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IntermediateResult implements Serializable {

	private static final long serialVersionUID = -7821393773189309259L;

	// attributes
	
	public String gameId;
	public String playerId;
	public int aimPosition;
	public boolean fieldIsOccupied;

	public List<String> selectedCards;
	public List<String> selectedTiles;
	public String selectedFigure;
	
	public List<String> playedCars;
	public List<String> playedTiles;
	
	public int costs;
	
	public boolean reject;

	// constructor
	
	public IntermediateResult() {
		this.selectedCards = new ArrayList<>();
		this.selectedTiles = new ArrayList<>();
		this.reject = false;
		this.playedCars = new ArrayList<>();
		this.playedTiles = new ArrayList<>();
		this.costs = 0;
	}

	// getters/setters
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public int getAimPosition() {
		return aimPosition;
	}

	public void setAimPosition(int aimPosition) {
		this.aimPosition = aimPosition;
	}

	public boolean isFieldIsOccupied() {
		return fieldIsOccupied;
	}

	public void setFieldIsOccupied(boolean fieldIsOccupied) {
		this.fieldIsOccupied = fieldIsOccupied;
	}
	
	public List<String> getSelectedCards() {
		return selectedCards;
	}

	public void setSelectedCards(List<String> selectedCards) {
		this.selectedCards = selectedCards;
	}

	public List<String> getSelectedTiles() {
		return selectedTiles;
	}

	public void setSelectedTiles(List<String> selectedTiles) {
		this.selectedTiles = selectedTiles;
	}

	public List<String> getPlayedTiles() {
		return playedTiles;
	}

	public void setPlayedTiles(List<String> playedTiles) {
		this.playedTiles = playedTiles;
	}

	public boolean isReject() {
		return reject;
	}

	public void setReject(boolean reject) {
		this.reject = reject;
	}

	public List<String> getPlayedCars() {
		return playedCars;
	}

	public void setPlayedCars(List<String> playedCars) {
		this.playedCars = playedCars;
	}

	public String getSelectedFigure() {
		return selectedFigure;
	}

	public void setSelectedFigure(String selectedFigure) {
		this.selectedFigure = selectedFigure;
	}

	public int getCosts() {
		return costs;
	}

	public void setCosts(int costs) {
		this.costs = costs;
	}
}
