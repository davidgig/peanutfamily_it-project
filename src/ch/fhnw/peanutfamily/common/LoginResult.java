package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.List;

public class LoginResult implements Serializable{

	private static final long serialVersionUID = -5225739992465086502L;
	
	// attributes
	
	private User user;
	private List<Game> savedGames;
	private List<Game> newGames;

	// constructors
	
	public LoginResult() {

	}

	public LoginResult(User user, List<Game> savedGames, List<Game> newGames) {
		this.user = user;
		this.savedGames = savedGames;
		this.newGames = newGames;
	}

	// getters/setters
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Game> getSavedGames() {
		return savedGames;
	}

	public void setSavedGames(List<Game> savedGames) {
		this.savedGames = savedGames;
	}

	public List<Game> getNewGames() {
		return newGames;
	}

	public void setNewGames(List<Game> newGames) {
		this.newGames = newGames;
	}
}
