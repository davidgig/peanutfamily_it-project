package ch.fhnw.peanutfamily.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GameBoard implements Serializable {

	private static final long serialVersionUID = -6651942705277250247L;

	// attributes
	
	private Game game;
	
	private List<TileStack> stacks;
	private List<Card> cards;
	private List<Figure> figures;
	private List<PlayerHand> playerHands;
	
	// constructors
	
	public GameBoard() {
		this.playerHands = new ArrayList<>();
		this.figures = new ArrayList<>();
		this.cards = new ArrayList<>();
	}

	public GameBoard(Game game, List<TileStack> stacks, List<Card> cards) {
		this.game = game;
		this.stacks = stacks;
		this.cards = cards;
		this.playerHands = new ArrayList<>();
		this.figures = new ArrayList<>();
	}

	// getter/setters
	
	public List<TileStack> getStacks() {
		return stacks;
	}

	public void setStacks(List<TileStack> stacks) {
		this.stacks = stacks;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public List<PlayerHand> getPlayerHands() {
		return playerHands;
	}

	public void setPlayerHands(List<PlayerHand> playerHands) {
		this.playerHands = playerHands;
	}

	public List<Figure> getFigures() {
		return figures;
	}

	public void setFigures(List<Figure> figures) {
		this.figures = figures;
	}
	
	/*
	 * This method makes a deep copy of the GameBoard without
	 * any references
	 */
	public static GameBoard copy(GameBoard other) {

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(other);
			oos.flush();
			oos.close();
			bos.close();
			byte[] byteData = bos.toByteArray();
			
			ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
			return (GameBoard) new ObjectInputStream(bais).readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
   }
}
