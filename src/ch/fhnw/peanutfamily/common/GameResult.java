package ch.fhnw.peanutfamily.common;

import java.io.Serializable;

public class GameResult implements Serializable {

	// attributes

	private boolean isWinner;
	private int score;

	// constructors

	public GameResult() {

	}

	// getters/setter

	public int getScore() {
		return score;
	}

	public boolean isWinner() {
		return isWinner;
	}

	public void setWinner(boolean isWinner) {
		this.isWinner = isWinner;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
