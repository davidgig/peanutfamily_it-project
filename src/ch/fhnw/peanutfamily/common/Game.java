package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.UUID;

public class Game implements Serializable{

	private static final long serialVersionUID = -208334372020504027L;

	// attribute
	
	private String id;
	private String name;
	private int amountOfPlayers;
	private int state;

	// constructors
	
	public Game() {
		this.id = UUID.randomUUID().toString();
	}

	public Game(String name, int amountOfPlayers, int state) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.amountOfPlayers = amountOfPlayers;
		this.state = state;
	}

	// getters/setters
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAmountOfPlayers() {
		return amountOfPlayers;
	}

	public void setAmountOfPlayers(int amountOfPlayers) {
		this.amountOfPlayers = amountOfPlayers;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		
		return id.toString();
	}
}
