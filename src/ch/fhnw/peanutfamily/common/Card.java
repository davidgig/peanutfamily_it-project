package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.UUID;

public class Card implements Serializable {

	private static final long serialVersionUID = 7695522086353127727L;
	
	// attributes

	private String id;
	private CardColor cardColor;
	private String fkGameId;
	private String fkPlayerId;

	// constructors

	public Card() {
		this.id = UUID.randomUUID().toString();
	}

	public Card(CardColor cardColor, String fkGameId) {
		this.id = UUID.randomUUID().toString();
		this.cardColor = cardColor;
		this.fkGameId = fkGameId;
	}

	// getters/setters

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CardColor getCardColor() {
		return cardColor;
	}

	public void setCardColor(CardColor cardColor) {
		this.cardColor = cardColor;
	}

	public String getFkGameId() {
		return fkGameId;
	}

	public void setFkGameId(String fkGameId) {
		this.fkGameId = fkGameId;
	}

	public String getFkPlayerId() {
		return fkPlayerId;
	}

	public void setFkPlayerId(String fkPlayerId) {
		this.fkPlayerId = fkPlayerId;
	}

	/*
	 * All possible card collors
	 */
	public static enum CardColor {
		BLUE, BROWN, GREEN, GREY, PINK, WHITE, YELLOW
	}
}
