package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlayerHand implements Serializable{

	private static final long serialVersionUID = 3350696172993122895L;

	// attributes
	
	Player player;
	List<Tile> myTiles;
	List<Card> myCards;

	// constructors
	
	public PlayerHand() {
		this.myTiles = new ArrayList<>();
		this.myCards = new ArrayList<>();
	}
	
	// getters/setters
	
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}



	public List<Tile> getMyTiles() {
		return myTiles;
	}

	public void setMyTiles(List<Tile> myTiles) {
		this.myTiles = myTiles;
	}

	public List<Card> getMyCards() {
		return myCards;
	}

	public void setMyCards(List<Card> myCards) {
		this.myCards = myCards;
	}
	
	public static PlayerHand copy(PlayerHand other) {
		PlayerHand newPlayerHand = new PlayerHand();
		newPlayerHand.setMyCards(other.getMyCards());
		newPlayerHand.setMyTiles(other.getMyTiles());
		newPlayerHand.setPlayer(other.getPlayer());
		return newPlayerHand;
	}
}
