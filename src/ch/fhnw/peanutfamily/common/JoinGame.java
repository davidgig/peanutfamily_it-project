package ch.fhnw.peanutfamily.common;

import java.io.Serializable;

public class JoinGame implements Serializable{

	private static final long serialVersionUID = -9110170599229035940L;
	
	// attributes
	
	private User myUser;
	private String gameId;
	
	// constructors
	
	public JoinGame() {

	}

	public JoinGame(User myUser, String gameId) {
		this.myUser = myUser;
		this.gameId = gameId;
	}

	// getters/setters
	
	public User getMyUser() {
		return myUser;
	}

	public void setMyUser(User myUser) {
		this.myUser = myUser;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
}
