package ch.fhnw.peanutfamily.common;

import java.io.Serializable;

public class PlayerInfo implements Serializable{

	private static final long serialVersionUID = -4455593090348126379L;

	// attributes
	
	private Player player;
	private Game game;

	private String selectedTile;
	
	// constructors
	
	public PlayerInfo() {

	}
	
	// getters/setters

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getSelectedTile() {
		return selectedTile;
	}

	public void setSelectedTile(String selectedTile) {
		this.selectedTile = selectedTile;
	}
}
