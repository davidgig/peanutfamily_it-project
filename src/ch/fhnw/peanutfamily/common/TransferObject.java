package ch.fhnw.peanutfamily.common;

import java.io.Serializable;

public class TransferObject implements Serializable {

	private static final long serialVersionUID = 3892339901532977068L;

	// attributes
	
	private TransferType transferType;
	private Object transferObject;
	private PlayerStatus playerStatus;
	
	// constructors
	
	public TransferObject() {
		playerStatus = PlayerStatus.EMPTY;
	}

	public TransferObject(TransferType transferType) {

		this.transferType = transferType;
	}

	// getters/setters
	
	public TransferType getTransferType() {
		return transferType;
	}

	public void setTransferType(TransferType transferType) {
		this.transferType = transferType;
	}

	public Object getTransferObject() {
		return transferObject;
	}

	public void setTransferObject(Object transferObject) {
		this.transferObject = transferObject;
	}

	public PlayerStatus getPlayerStatus() {
		return playerStatus;
	}

	public void setPlayerStatus(PlayerStatus playerStatus) {
		this.playerStatus = playerStatus;
	}

	/*
	 * TransferType stands for a action which will be executes on the server or cliet side 
	 */
	public static enum TransferType {
		CONNECTION_ESTABLISHED, LOGIN, LOGIN_RESULT, LOGIN_SUCCESS, LOGIN_FAIL, REGISTER, 
		REGISTER_RESULT, REGISTER_SUCCESS, REGISTER_FAIL, CREATEGAME, CREATEGAMESUCCESS,
		NEWCREATEDGAME, INITGAMEBOARD, MESSAGE, JOINGAME, MAKEAMOVE, MAKEAMOVERESULT, 
		PLAYERACTION, ENDROUND, BUYCARD, INTERMEDIATERESULT, FINISHED
	}
	
	/*
	 * TransferStatus tells the client have state he has and what he can do
	 */
	public static enum PlayerStatus {
		EMPTY, WAIT, READY, INTERMEDIATECOSTS, INTERMEDIATEOCCUPIED, MOVEMADE, END
	}
}
