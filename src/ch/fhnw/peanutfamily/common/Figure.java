package ch.fhnw.peanutfamily.common;

import java.io.Serializable;
import java.util.UUID;

public class Figure implements Serializable {

	private static final long serialVersionUID = -6940884087720652293L;

	// attributes

	private String id;
	private int number;
	private int tileStackPosition;
	private String color;
	private String fkPlayer;

	// constructors

	public Figure() {
		this.id = UUID.randomUUID().toString();
		this.tileStackPosition = 0;
	}

	// getters/setters

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getFkPlayer() {
		return fkPlayer;
	}

	public void setFkPlayer(String fkPlayer) {
		this.fkPlayer = fkPlayer;
	}

	public int getTileStackPosition() {
		return tileStackPosition;
	}

	public void setTileStackPosition(int tileStackPosition) {
		this.tileStackPosition = tileStackPosition;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public static Figure copy(Figure other) {
		Figure newFigure = new Figure();
		newFigure.setColor(other.getColor());
		newFigure.setFkPlayer(other.fkPlayer);
		newFigure.setId(other.getId());
		newFigure.setNumber(other.getNumber());
		newFigure.setTileStackPosition(other.getTileStackPosition());
		return newFigure;
	}
}
