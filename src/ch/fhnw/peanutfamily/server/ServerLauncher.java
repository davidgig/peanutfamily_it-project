package ch.fhnw.peanutfamily.server;

import ch.fhnw.peanutfamily.database.DatabaseManager;

/**
 * The ServerLauncher starts the server application and
 * manages the database
 * 
 * @author David
 *
 */
public class ServerLauncher {
	
	/**
	 * This method start the Thread ServerManager and runs the db scripts
	 * 
	 * @param args the command line arguments
	 * 
	 * @author David
	 */
	public static void main(String[] args) {
		
		Thread t = new Thread(new ServerManager());
		t.start();
		
		DatabaseManager databaseManager = new DatabaseManager();
    }
}
