package ch.fhnw.peanutfamily.server;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import ch.fhnw.peanutfamily.common.TransferObject;

/**
 * The ServerSender send the TransferObject to one or more Users
 * 
 * @author David
 *
 */
public class ServerSender {

	/**
	 * This method send information to one user
	 * 
	 * @param serverConnection the connection to the specific user
	 * @param transerObject the object containing the information
	 * 
	 * @author David
	 */
	public void send(ServerConnection serverConnection, TransferObject transerObject) {

		try {
			serverConnection.getObjectOutputStream().writeObject(transerObject);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method send information to all users
	 * 
	 * @param userServerConnections the connections to all users
	 * @param transferObject the object containing the information
	 * 
	 * @author David
	 */
	public void sendAll(Map<String, ServerConnection> userServerConnections, TransferObject transferObject) {

		for (Entry<String, ServerConnection> entry : userServerConnections.entrySet()) {

			try {
				entry.getValue().getObjectOutputStream().writeObject(transferObject);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
