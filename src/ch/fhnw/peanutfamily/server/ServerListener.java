package ch.fhnw.peanutfamily.server;

import java.io.IOException;

import ch.fhnw.peanutfamily.common.TransferObject;

/**
 * This method is listening for input which is send by the users and
 * forwards them to the ServerGameLogic
 * 
 * @author David
 *
 */
public class ServerListener implements Runnable {

	private TransferObject transferObject;
	private ServerConnection serverConnection;
	private ServerGameLogic gameLogic;

	/**
	 * Constructor getting a ServerGameLogic instance
	 * 
	 * @param serverConnection
	 */
	public ServerListener(ServerConnection serverConnection) {

		this.serverConnection = serverConnection;
		this.gameLogic = ServerGameLogic.getInstance();
	}

	/**
	 * This method listens for input from Users
	 * 
	 * @author David
	 */
	@Override
	public void run() {

		System.out.println(serverConnection.getSocket().getPort() + "is running");

		while (true) {
			if (serverConnection.getSocket().isConnected()) {
				try {
					
					// read transferObject from input stream
					transferObject = (TransferObject) serverConnection.getObjectInputStream().readObject();

					// executes the game logic
					if (transferObject != null) {
						gameLogic.executeEvent(transferObject, serverConnection);
					}
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
