package ch.fhnw.peanutfamily.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import ch.fhnw.peanutfamily.common.Card;
import ch.fhnw.peanutfamily.common.Card.CardColor;
import ch.fhnw.peanutfamily.common.CreateGame;
import ch.fhnw.peanutfamily.common.Figure;
import ch.fhnw.peanutfamily.common.Game;
import ch.fhnw.peanutfamily.common.GameBoard;
import ch.fhnw.peanutfamily.common.GameResult;
import ch.fhnw.peanutfamily.common.IntermediateResult;
import ch.fhnw.peanutfamily.common.JoinGame;
import ch.fhnw.peanutfamily.common.LoginResult;
import ch.fhnw.peanutfamily.common.MakeAMove;
import ch.fhnw.peanutfamily.common.Player;
import ch.fhnw.peanutfamily.common.PlayerHand;
import ch.fhnw.peanutfamily.common.PlayerInfo;
import ch.fhnw.peanutfamily.common.Tile;
import ch.fhnw.peanutfamily.common.Tile.TileColor;
import ch.fhnw.peanutfamily.common.Tile.TileNumber;
import ch.fhnw.peanutfamily.common.TileStack;
import ch.fhnw.peanutfamily.common.TransferObject;
import ch.fhnw.peanutfamily.common.TransferObject.PlayerStatus;
import ch.fhnw.peanutfamily.common.TransferObject.TransferType;
import ch.fhnw.peanutfamily.common.User;
import ch.fhnw.peanutfamily.common.UserData;
import ch.fhnw.peanutfamily.database.GameDaoImpl;
import ch.fhnw.peanutfamily.database.PlayerDaoImpl;
import ch.fhnw.peanutfamily.database.UserDaoImpl;

/**
 * The ServerGameLogic contains the whole server-side functionality of the game
 * 
 * @author Daniel, David, Ali
 *
 */
public class ServerGameLogic {

	// instance
	private static ServerGameLogic instance = null;

	private Map<String, GameBoard> gameBoards = new HashMap<>();

	private ServerConnection serverConnection;
	private ServerSender serverSender = new ServerSender();

	private Map<String, ServerConnection> userSeverConnections = new HashMap<>();

	/**
	 * This function returns the ServerGameLogic instance (Singleton)
	 * 
	 * @return instance of ServerGameLogic
	 * 
	 * @author Daniel
	 */
	public static ServerGameLogic getInstance() {
		if (instance == null) {
			instance = new ServerGameLogic();
		}
		return instance;
	}

	/**
	 * This method executes the game functions according to the type of the
	 * TransferObject
	 * 
	 * @param transferObject
	 * @param serverConnection
	 * 
	 * @author Daniel
	 */
	public void executeEvent(TransferObject transferObject, ServerConnection serverConnection) {

		this.serverConnection = serverConnection;

		switch (transferObject.getTransferType()) {
		case LOGIN:
			login((UserData) transferObject.getTransferObject());
			break;
		case REGISTER:
			register((UserData) transferObject.getTransferObject());
			break;
		case CREATEGAME:
			createGame((CreateGame) transferObject.getTransferObject());
			break;
		case JOINGAME:
			joinGame((JoinGame) transferObject.getTransferObject());
			break;
		case MESSAGE:
			sendMessage((String) transferObject.getTransferObject());
			break;
		case MAKEAMOVE:
			makeAMove((MakeAMove) transferObject.getTransferObject());
			break;
		case ENDROUND:
			endRound((PlayerInfo) transferObject.getTransferObject());
			break;
		case BUYCARD:
			buyCard((PlayerInfo) transferObject.getTransferObject());
			break;
		case INTERMEDIATERESULT:
			continueMove((IntermediateResult) transferObject.getTransferObject(), transferObject.getPlayerStatus());
			break;
		default:
		}
	}

	/**
	 * This method creates all GameBoard data which is player specific
	 * 
	 * @param gameId
	 * @param player
	 * 
	 * @author Daniel
	 */
	private void addPlayerInfosToGameBoard(String gameId, Player player) {

		PlayerHand playerHand = new PlayerHand();
		playerHand.setPlayer(player);

		GameBoard gameBoard = GameBoard.copy(gameBoards.get(gameId));

		// add player cards
		List<Card> cards = gameBoard.getCards();

		for (int i = 0; i < 5; i++) {
			Card card = cards.get(0);
			card.setFkGameId("");
			card.setFkPlayerId(player.getId());

			playerHand.getMyCards().add(card);

			// remove from card stack after put to PlayerHand
			cards.remove(0);
		}

		// add figures
		for (int i = 0; i < 3; i++) {
			Figure figure = new Figure();
			figure.setNumber(i + 1);
			figure.setColor(player.getColor());
			figure.setFkPlayer(player.getId());

			gameBoard.getFigures().add(figure);
		}

		// stores card stack and PlayerHand
		gameBoard.setCards(cards);
		gameBoard.getPlayerHands().add(playerHand);
		gameBoards.put(gameId, gameBoard);
	}

	/**
	 * This method handles the whole process of buying cards by exchange it with
	 * a tile
	 * 
	 * @param playerInfo
	 * 
	 * @author Daniel
	 */
	private void buyCard(PlayerInfo playerInfo) {

		GameBoard gameBoard = GameBoard.copy(gameBoards.get(playerInfo.getGame().getId()));
		PlayerHand myPlayerHand = null;
		Tile selectedTile = null;

		// find my playeHand
		for (PlayerHand playerHand : gameBoard.getPlayerHands()) {
			if (playerHand.getPlayer().getId().equals(playerInfo.getPlayer().getId())) {
				myPlayerHand = playerHand;
			}
		}

		// find the selectedTile from my playerHand
		for (Tile tile : myPlayerHand.getMyTiles()) {
			if (tile.getId().equals(playerInfo.getSelectedTile())) {
				selectedTile = tile;
			}
		}

		// remove tile from playerHand
		myPlayerHand.getMyTiles().remove(selectedTile);

		// calculate amount of cards
		int tileValue = selectedTile.getTileNumber().getValue();
		int calculatedTileValue = tileValue / 2;

		// if number of tile was 1, it should be 0 cards
		if (calculatedTileValue == 0) {
			calculatedTileValue = 1;
		}

		// add amount of new cards to playerHand
		for (int i = 0; i < calculatedTileValue; i++) {
			Card newCard = gameBoard.getCards().get(0);
			myPlayerHand.getMyCards().add(newCard);
			gameBoard.getCards().remove(newCard);
		}

		// save changes
		gameBoards.put(playerInfo.getGame().getId(), gameBoard);

		// load changed data and send it to player
		loadGameBoardForPlayer(playerInfo.getGame().getId(), playerInfo.getPlayer().getId(), true, PlayerStatus.READY);
	}

	/**
	 * This method checks if water is between the start position and the target
	 * position and calculates the value to cross it
	 * 
	 * @author David
	 * 
	 * @param tileStacks
	 * @param aimPosition
	 * @param startPosition
	 */
	private int checkForWater(List<TileStack> tileStacks, int aimPosition, int startPosition) {

		int costs = 0;

		int costTileBefore = 0;
		int costTileAfter = 0;

		boolean startCounting = false;

		for (int i = startPosition + 1; i <= aimPosition; i++) {

			TileStack tileStack = tileStacks.get(i);

			// first water -> start counting
			if (tileStack.isWater() && !startCounting) {

				if (tileStacks.get(i - 1).isAtlantis()) {
					costTileBefore = 0;
				} else {
					costTileBefore = tileStacks.get(i - 1).getTiles().get(0).getTileNumber().getValue();
				}

				startCounting = true;
			}

			// not water anymore -> stop counting
			if (!tileStack.isWater() && startCounting) {

				if (tileStacks.get(i).isLand()) {
					costTileAfter = 0;
				} else {
					costTileAfter = tileStacks.get(i).getTiles().get(0).getTileNumber().getValue();
				}
				costs += costTileBefore < costTileAfter ? costTileBefore : costTileAfter;
				startCounting = false;
			}
		}

		return costs;
	}

	/**
	 * This method checks if the target position of the figure is already
	 * occupied or not
	 * 
	 * @param aimPosition
	 * @param tileStacks
	 * @param figures
	 * @return true if occupied
	 * 
	 * @author Daniel
	 */
	private boolean checkIfOccupied(int aimPosition, List<TileStack> tileStacks, List<Figure> figures) {

		boolean isOccupied = false;

		TileStack collectedTileStack = tileStacks.get(aimPosition);

		// check if is land -> return false
		if (collectedTileStack.isLand()) {
			return isOccupied;
		}

		// check if another figure is on this field
		for (Figure figure : figures) {
			if (figure.getTileStackPosition() == collectedTileStack.getPosition()) {
				isOccupied = true;
			}
		}

		return isOccupied;
	}

	/**
	 * This function continues the move the player did after playing another
	 * card (field was occupied) or after he has payed to cross the water
	 * 
	 * @param intermediateResult
	 * @param playerStatus
	 * 
	 * @author Daniel
	 */
	private void continueMove(IntermediateResult intermediateResult, PlayerStatus playerStatus) {

		List<String> selectedCardsId = intermediateResult.getPlayedCars();
		selectedCardsId.addAll(intermediateResult.getSelectedCards());

		List<String> selectedTilesId = intermediateResult.getPlayedTiles();
		selectedTilesId.addAll(intermediateResult.getSelectedTiles());

		// continue move functionality
		doPlayAction(playerStatus, intermediateResult, false, intermediateResult.getGameId(),
				intermediateResult.getPlayerId(), selectedCardsId, selectedTilesId,
				intermediateResult.getSelectedFigure(), intermediateResult.getAimPosition());
	}

	/**
	 * This method creates a new Game including an Player and stores them on the
	 * database. It also creates the GameBoard and all player information and
	 * returns them to the User.
	 * 
	 * @param createGame
	 * 
	 * @author Daniel
	 */
	private void createGame(CreateGame createGame) {

		GameDaoImpl gameDaoImpl = new GameDaoImpl();
		PlayerDaoImpl playerDaoImpl = new PlayerDaoImpl();

		// create gamae
		Game newGame = new Game();
		newGame.setName(createGame.getGameName());
		newGame.setState(0);
		newGame.setAmountOfPlayers(createGame.getAmountOfPlayers());
		gameDaoImpl.create(newGame);

		// create player
		Player newPlayer = new Player();
		newPlayer.setColor("red");
		newPlayer.setFkGameId(newGame.getId());
		newPlayer.setFkUserId(createGame.getMyUser().getId());
		playerDaoImpl.create(newPlayer);

		// send new game to all users
		TransferObject transferObject = new TransferObject(TransferType.NEWCREATEDGAME);
		serverSender.sendAll(userSeverConnections, transferObject);

		// init gameboard and player infos
		initGameBoard(newGame);
		addPlayerInfosToGameBoard(newGame.getId(), newPlayer);

		// load GameBoard for Player
		loadGameBoardForPlayer(newGame.getId(), newPlayer.getId(), true, PlayerStatus.EMPTY);
	}

	/**
	 * This method does the whole move of the player, after it did changes will
	 * be send back to the player. if costs for crossing water or aim position
	 * is occupied there will be a return with the IntermediateResult which
	 * later can be continued
	 * 
	 * @param playerStatus
	 * @param intermediateResult
	 * @param firstTime
	 * @param gameId
	 * @param playerId
	 * @param selectedCardsId
	 * @param selectedTilesId
	 * @param selectedFigureId
	 * @param aimPosition
	 * 
	 * @author David
	 */
	private void doPlayAction(PlayerStatus playerStatus, IntermediateResult intermediateResult, boolean firstTime,
			String gameId, String playerId, List<String> selectedCardsId, List<String> selectedTilesId,
			String selectedFigureId, int aimPosition) {

		// create or update intermediateResult
		if (firstTime) {
			intermediateResult = new IntermediateResult();
			intermediateResult.setGameId(gameId);
			intermediateResult.setPlayerId(playerId);
			intermediateResult.setSelectedFigure(selectedFigureId);
		} else {
			intermediateResult.setCosts(0);
		}

		GameBoard gameBoard = GameBoard.copy(gameBoards.get(gameId));

		List<TileStack> tileStacks = gameBoard.getStacks();
		List<Figure> figures = gameBoard.getFigures();

		// find playerHand
		PlayerHand myPlayerHand = getPlayerHand(gameBoard, playerId);

		List<Card> myCards = myPlayerHand.getMyCards();
		List<Tile> myTiles = myPlayerHand.getMyTiles();

		// get selected figure
		Figure selectedFigure = getSelectedFigure(figures, selectedFigureId);

		// get selected cards
		List<Card> selectedCards = new ArrayList<>();
		for (String selectedCard : selectedCardsId) {
			selectedCards.add(getSelectedCard(myCards, selectedCard));
		}

		// get selected tiles
		List<Tile> selectedTiles = new ArrayList<>();
		if (selectedTilesId != null) {
			for (String selectedTile : selectedTilesId) {
				selectedTiles.add(getSelectedTile(myTiles, selectedTile));
			}
		}

		if (playerStatus != PlayerStatus.INTERMEDIATECOSTS) {

			int oldAimPosition = 0;

			/*
			 * if it is a new move, the old position is the position of the
			 * figure, otherwise it will be the position wich was calculated
			 * before
			 */
			if (firstTime) {
				oldAimPosition = selectedFigure.getTileStackPosition();
			} else {
				oldAimPosition = aimPosition;
			}

			// locate aim stack position
			aimPosition = locatePosition(selectedFigure, selectedCards.get(selectedCards.size() - 1), tileStacks,
					oldAimPosition);

			// check if water is between
			int costs = checkForWater(tileStacks, aimPosition, oldAimPosition);

			// return -> water costs
			if (costs > 0) {

				intermediateResult.setCosts(intermediateResult.getCosts() + costs);
				intermediateResult.getPlayedCars().addAll(selectedCardsId);
				intermediateResult.setAimPosition(aimPosition);
				if (selectedTilesId != null) {
					intermediateResult.getPlayedTiles().addAll(selectedTilesId);
				}
				sendIntermediateResult(intermediateResult, PlayerStatus.INTERMEDIATECOSTS);
				return;
			}
		}

		// check aim position if occupied
		boolean isOccupied = checkIfOccupied(aimPosition, tileStacks, figures);

		// return -> play another card
		if (isOccupied) {

			intermediateResult.setAimPosition(aimPosition);
			intermediateResult.setFieldIsOccupied(isOccupied);
			intermediateResult.getPlayedCars().add(selectedCards.get(selectedCards.size() - 1).getId());

			sendIntermediateResult(intermediateResult, PlayerStatus.INTERMEDIATEOCCUPIED);
			return;
		}

		// check aim position if occupied
		boolean fieldBehind = checkIfOccupied(aimPosition - 1, tileStacks, figures);
		if (tileStacks.get(aimPosition - 1).isWater()) {
			fieldBehind = true;
		}

		// find tile to collect
		Tile collectedTile = getCollectedTile(fieldBehind, aimPosition, figures, tileStacks, selectedFigure);

		// change selected figure
		selectedFigure.setTileStackPosition(aimPosition);

		// remove selected cards
		for (Card selectedCard : selectedCards) {
			myCards.remove(selectedCard);
		}

		// remove selected tiles
		for (Tile selectedTile : selectedTiles) {
			myTiles.remove(selectedTile);
		}

		// add tile to hand
		if (collectedTile != null) {
			myTiles.add(collectedTile);
		}

		// add new card
		Card card = gameBoard.getCards().get(0);
		myCards.add(card);
		gameBoard.getCards().remove(card);

		gameBoards.put(gameId, gameBoard);

		loadGameBoardForPlayer(gameId, playerId, false, PlayerStatus.MOVEMADE);
	}

	/**
	 * This method ends the round of the player by informing all others and
	 * update their gameBoard data, it also checks how the next player is and
	 * inform him about this too.
	 * 
	 * @param playerInfo
	 * 
	 * @author Daniel
	 */
	private void endRound(PlayerInfo playerInfo) {

		GameBoard gameBoard = gameBoards.get(playerInfo.getGame().getId());
		List<PlayerHand> playerHands = gameBoard.getPlayerHands();

		// check if player is finish
		boolean isFinished = checkIfGameIsFinished(gameBoard, playerInfo);

		if (isFinished) {

			// finish game
			finishGame(gameBoard);
		} else {

			// inform others
			for (PlayerHand hand : playerHands) {
				if (hand.getPlayer().getId() != playerInfo.getPlayer().getId()) {
					serverConnection = userSeverConnections.get(hand.getPlayer().getFkUserId());
					loadGameBoardForPlayer(playerInfo.getGame().getId(), hand.getPlayer().getId(), true,
							PlayerStatus.WAIT);
				}
			}

			int playerPosition = 0;
			int nextPlayerid = 0;

			// get next player
			for (PlayerHand playerHand : playerHands) {
				if (playerHand.getPlayer().getId().equals(playerInfo.getPlayer().getId())) {
					playerPosition = playerHands.indexOf(playerHand);
				}
			}

			// if last of list it begins at the first player again
			if (playerPosition != playerHands.size() - 1) {
				nextPlayerid = playerPosition + 1;
			} else {
				nextPlayerid = 0;
			}

			String nextPlayer = playerHands.get(nextPlayerid).getPlayer().getFkUserId();

			// inform next player
			serverConnection = userSeverConnections.get(nextPlayer);
			TransferObject transferObject = new TransferObject(TransferType.PLAYERACTION);
			transferObject.setPlayerStatus(PlayerStatus.READY);

			serverSender.send(serverConnection, transferObject);
		}
	}

	/**
	 * This method checks if the player has all his figures on the land tile
	 * 
	 * @param gameBoard
	 * @param playerInfo
	 * @return true if finished
	 * 
	 * @author David
	 */
	private boolean checkIfGameIsFinished(GameBoard gameBoard, PlayerInfo playerInfo) {

		boolean isFinished = true;
		String playerId = playerInfo.getPlayer().getId();

		// returns false if one is not on the land tile
		for (Figure figure : gameBoard.getFigures()) {
			if (figure.getFkPlayer().equals(playerId) && figure.getTileStackPosition() != 54) {
				isFinished = false;
			}
		}

		return isFinished;
	}

	/**
	 * This method ends the game by calculating the score of the players and
	 * inform them about the result. game will be finished
	 * 
	 * @param gameBoard
	 * 
	 * @author David
	 */
	private void finishGame(GameBoard gameBoard) {

		UserDaoImpl userDaoImpl = new UserDaoImpl();
		
		Map<String, Integer> playerScores = new HashMap<>();

		List<TileStack> tileStacks = gameBoard.getStacks();

		for (PlayerHand playerHand : gameBoard.getPlayerHands()) {

			int score = 0;

			// + points from cards
			score += playerHand.getMyCards().size();

			// + points from tiles
			for (Tile tile : playerHand.getMyTiles()) {
				score += tile.getTileNumber().getValue();
			}

			// - points to cross water
			for (Figure figure : gameBoard.getFigures()) {
				if (figure.getTileStackPosition() != 54) {
					if (figure.getFkPlayer().equals(playerHand.getPlayer().getId())) {
						score -= checkForWater(tileStacks, 54, figure.getTileStackPosition());
					}
				}
			}

			playerScores.put(playerHand.getPlayer().getFkUserId(), score);
		}

		// send result to the players
		for (Map.Entry<String, Integer> entry : playerScores.entrySet()) {
			
			String userId = entry.getKey();
			int highscore = entry.getValue();
			
			serverConnection = userSeverConnections.get(userId);
			
			//check if winner
			boolean isWinner = checkIfWinner(playerScores, userId);
			
			// update user
			User user = (User) userDaoImpl.read(userId);
			if(user.getHighscore() < highscore) {
				user.setHighscore(highscore);
			}
			if(isWinner) {
				user.setVictories(user.getVictories() + 1);
			}
			userDaoImpl.update(user);
			
			// inform player
			GameResult gameResult = new GameResult();
			gameResult.setScore(entry.getValue());
			gameResult.setWinner(isWinner);
			
			TransferObject transferObject = new TransferObject(TransferType.FINISHED);
			transferObject.setPlayerStatus(PlayerStatus.END);
			transferObject.setTransferObject(gameResult);
			
			serverSender.send(serverConnection, transferObject);
		}
	}
	
	/**
	 * This method checks if the player is the winner or not
	 * 
	 * @param scores
	 * @param userId
	 * @return true if winner
	 * 
	 * @author David
	 */
	private boolean checkIfWinner(Map<String, Integer> scores, String userId) {
		
		boolean result = true;
		
		int score = scores.get(userId);
		
		for (Map.Entry<String, Integer> entry : scores.entrySet()) {
			if(score < entry.getValue()) {
				result = false;
			}
		}
		
		return result;
	}

	/**
	 * This method gets next free tile behind the figures new position and adds
	 * it to the PlayerHand
	 * 
	 * @param isOccupied
	 * @param aimPosition
	 * @param figures
	 * @param tileStacks
	 * @return the tile to collect
	 * 
	 * @author David
	 */
	private Tile getCollectedTile(boolean isOccupied, int aimPosition, List<Figure> figures, List<TileStack> tileStacks,
			Figure selectedFigure) {

		Tile collectedTile = null;
		TileStack collectedTileStack = tileStacks.get(aimPosition - 1);

		if (isOccupied) {

			int count = 1;
			boolean notEmpty = true;

			while (notEmpty) {

				// if position is 0 (atlantis) stops finding tile behind
				if ((aimPosition - count) == 0) {
					break;
				}

				collectedTileStack = tileStacks.get(aimPosition - count);

				// if water continues search
				if (collectedTileStack.isWater()) {
					count++;
					continue;
				}

				notEmpty = false;

				// if figure is on field it continues search
				for (Figure figure : figures) {
					if (!figure.getId().equals(selectedFigure.getId())) {
						if (figure.getTileStackPosition() == collectedTileStack.getPosition()) {
							notEmpty = true;
						}
					}
				}

				count++;
			}
		}

		// takes the tile top of the tileStack if there is one
		if (collectedTileStack.getTiles().size() > 0) {
			collectedTile = collectedTileStack.getTiles().get(0);
			collectedTileStack.getTiles().remove(0);

			// no more tiles -> stacks is going to be water
			if (collectedTileStack.getTiles().size() == 0) {
				collectedTileStack.setWater(true);
			}
		}

		return collectedTile;
	}

	/**
	 * This method loads all new Games
	 * 
	 * @return list of games
	 * 
	 * @author Daniel
	 */
	private List<Game> getNewGames() {

		GameDaoImpl gameDaoImpl = new GameDaoImpl();

		List<Game> newGames = new ArrayList<>();

		// reads all games from the db
		List<Game> games = (List<Game>) gameDaoImpl.readAll();
		for (Game game : games) {
			if (game.getState() == 0) {
				newGames.add(game);
			}
		}

		return newGames;
	}

	/**
	 * This method gets the playerHand of a specific player
	 * 
	 * @param gameBoard
	 * @param playerId
	 * @return playerHand of the player
	 * 
	 * @author Daniel
	 */
	private PlayerHand getPlayerHand(GameBoard gameBoard, String playerId) {

		PlayerHand playerHandResult = null;

		for (PlayerHand playerHand : gameBoard.getPlayerHands()) {
			if (playerHand.getPlayer().getId().equals(playerId)) {
				playerHandResult = playerHand;
				break;
			}
		}

		return playerHandResult;
	}

	/**
	 * This method loads all saves Games ! (not needed right now)
	 * 
	 * @param user
	 * @return list of games�
	 * 
	 * @author Daniel
	 */
	private List<Game> getSaveGames(User user) {

		PlayerDaoImpl playerDaoImpl = new PlayerDaoImpl();
		GameDaoImpl gameDaoImpl = new GameDaoImpl();

		List<Player> userPlayers = new ArrayList<>();
		List<Game> savedGames = new ArrayList<>();

		// get all players of one user
		List<Player> players = (List<Player>) playerDaoImpl.readAll();
		for (Player player : players) {
			if (player.getFkUserId() == user.getId()) {
				userPlayers.add(player);
			}
		}

		// get each game for each player
		List<Game> games = (List<Game>) gameDaoImpl.readAll();
		for (Game game : games) {
			for (Player player : players) {
				if (game.getId() == player.getFkGameId()) {
					savedGames.add(game);
				}
			}
		}

		return savedGames;
	}

	/**
	 * This method returns the selected card from the PlayerHand cards
	 * 
	 * @param cards
	 * @param selectedCardId
	 * @return selected Card
	 * 
	 * @author Ali
	 */
	private Card getSelectedCard(List<Card> cards, String selectedCardId) {

		Card selectedCard = null;

		for (Card card : cards) {
			if (card.getId().equals(selectedCardId)) {
				selectedCard = card;
				break;
			}
		}

		return selectedCard;
	}

	/**
	 * This method return the selected figure from a list of figures
	 * 
	 * @param figures
	 * @param selectedFigureId
	 * @return selected figure
	 * 
	 * @author Ali
	 */
	private Figure getSelectedFigure(List<Figure> figures, String selectedFigureId) {

		Figure selectedFigure = null;

		for (Figure figure : figures) {
			if (figure.getId().equals(selectedFigureId)) {
				selectedFigure = figure;
				break;
			}
		}

		return selectedFigure;
	}

	/**
	 * This method return the selected tile from the PlayerHand tiles
	 * 
	 * @param tiles
	 * @param selectedTileId
	 * @return the selected Tile
	 * 
	 * @author Ali
	 */
	private Tile getSelectedTile(List<Tile> tiles, String selectedTileId) {

		Tile selectedTile = null;

		for (Tile tile : tiles) {
			if (tile.getId().equals(selectedTileId)) {
				selectedTile = tile;
				break;
			}
		}

		return selectedTile;
	}

	/**
	 * This method creates all cards for the gameBaord
	 * 
	 * @param gameId
	 * @return list of cards
	 * 
	 * @author Daniel
	 */
	private List<Card> initCards(String gameId) {

		List<Card> cards = new ArrayList<>();

		List<CardColor> cardColors = new ArrayList<>(Arrays.asList(CardColor.values()));

		for (CardColor cardColor : cardColors) {
			for (int i = 0; i < 15; i++) {
				Card card = new Card(cardColor, gameId);
				cards.add(card);
			}
		}

		Collections.shuffle(cards);

		return cards;
	}

	/**
	 * This method creates the gameBoard
	 * 
	 * @param game
	 * 
	 * @author Daniel
	 */
	private void initGameBoard(Game game) {

		// init tileStacks
		List<TileStack> tileStacks = initTileStacks(game.getId());

		// init cards
		List<Card> cards = initCards(game.getId());

		GameBoard gameBoard = new GameBoard(game, tileStacks, cards);
		gameBoards.put(game.getId(), gameBoard);
	}

	/**
	 * This method creates all tiles for the gameBoard
	 * 
	 * @return list of tiles
	 * 
	 * @author Daniel
	 */
	private List<Tile> initTiles() {

		List<Tile> tiles = new ArrayList<>();

		List<TileNumber> tileNumbers = new ArrayList<>(Arrays.asList(TileNumber.values()));

		// each tile is created once
		for (TileColor tileColor : TileColor.values()) {
			for (TileNumber tileNumber : tileNumbers) {
				Tile tile = new Tile(tileNumber, tileColor, "");
				tiles.add(tile);
			}
		}

		tileNumbers.remove(TileNumber.ONE);
		tileNumbers.remove(TileNumber.SEVEN);

		// all tiles except the once with 1 and 7 are created a second time
		for (TileColor tileColor : TileColor.values()) {
			for (TileNumber tileNumber : tileNumbers) {
				Tile tile = new Tile(tileNumber, tileColor, "");
				tiles.add(tile);
			}
		}

		Collections.shuffle(tiles);

		return tiles;
	}

	/**
	 * This method creates all tile stacks for the gameBoard
	 * 
	 * @param gameId
	 * @return list of tileStacks
	 * 
	 * @author Daniel
	 */
	private List<TileStack> initTileStacks(String gameId) {

		List<TileStack> tileStacks = new ArrayList();
		List<Tile> tiles = initTiles();

		// atlantis
		TileStack atlantisTileStack = new TileStack();
		atlantisTileStack.setFkGameId(gameId);
		atlantisTileStack.setPosition(0);
		atlantisTileStack.setAtlantis(true);
		tileStacks.add(atlantisTileStack);

		// first half
		initTilesX(tileStacks, gameId, tiles, 1, 10, 2);
		initTilesX(tileStacks, gameId, tiles, 11, 10, 1);
		initTilesX(tileStacks, gameId, tiles, 21, 6, 2);

		// water
		TileStack waterTileStack = new TileStack();
		waterTileStack.setFkGameId(gameId);
		waterTileStack.setPosition(27);
		waterTileStack.setWater(true);
		tileStacks.add(waterTileStack);

		// second half
		initTilesX(tileStacks, gameId, tiles, 28, 6, 2);
		initTilesX(tileStacks, gameId, tiles, 34, 10, 1);
		initTilesX(tileStacks, gameId, tiles, 44, 10, 2);

		// land
		TileStack landTileStack = new TileStack();
		landTileStack.setFkGameId(gameId);
		landTileStack.setPosition(54);
		landTileStack.setLand(true);
		tileStacks.add(landTileStack);

		return tileStacks;
	}

	/**
	 * This method creates a series of tiles (e.g. 10 times with 2 tiles) and
	 * calculated the position for each tileStack
	 * 
	 * @param tileStacks
	 * @param gameId
	 * @param tiles
	 * @param startNumber
	 * @param times
	 * @param amount
	 * 
	 * @author Daniel
	 */
	private void initTilesX(List<TileStack> tileStacks, String gameId, List<Tile> tiles, int startNumber, int times,
			int amount) {

		for (int i = startNumber; i < startNumber + times; i++) {

			TileStack tileStack = new TileStack();
			tileStack.setFkGameId(gameId);
			tileStack.setPosition(i);

			for (int j = 1; j <= amount; j++) {
				Tile tile = tiles.get(0);
				tile.setFkTileStackId(tileStack.getId());
				tileStack.getTiles().add(tiles.get(0));
				tiles.remove(0);
			}

			tileStacks.add(tileStack);
		}
	}

	/**
	 * This method creates a Player and all player information for an existing
	 * Game and returns the GameBoard and all player information to the user
	 * 
	 * @param joinGame
	 * 
	 * @author Daniel
	 */
	private void joinGame(JoinGame joinGame) {

		PlayerDaoImpl playerDaoImpl = new PlayerDaoImpl();
		GameDaoImpl gameDaoImpl = new GameDaoImpl();

		// create new Player
		Player newPlayer = new Player();
		newPlayer.setColor(getColorForPlayer(joinGame.getGameId()));
		newPlayer.setFkGameId(joinGame.getGameId());
		newPlayer.setFkUserId(joinGame.getMyUser().getId());
		playerDaoImpl.create(newPlayer);

		// create Player information and add them to the GameBoard
		addPlayerInfosToGameBoard(joinGame.getGameId(), newPlayer);

		// load and send the GameBoard to the new Player
		loadGameBoardForPlayer(joinGame.getGameId(), newPlayer.getId(), true, PlayerStatus.WAIT);

		// inform others
		for (PlayerHand hand : gameBoards.get(joinGame.getGameId()).getPlayerHands()) {

			if (hand.getPlayer().getId() != newPlayer.getId()) {

				serverConnection = userSeverConnections.get(hand.getPlayer().getFkUserId());
				loadGameBoardForPlayer(joinGame.getGameId(), hand.getPlayer().getId(), true, PlayerStatus.WAIT);
			}
		}

		// check if last player (yes = change game status and first user can
		// start)
		GameBoard gameBoard = GameBoard.copy(gameBoards.get(joinGame.getGameId()));
		if (gameBoard.getPlayerHands().size() == gameBoard.getGame().getAmountOfPlayers()) {

			// update game status
			Game game = gameBoard.getGame();
			game.setState(1);
			gameDaoImpl.update(game);
			gameBoards.put(game.getId(), gameBoard);

			// send first Player start permission
			serverConnection = userSeverConnections.get(gameBoard.getPlayerHands().get(0).getPlayer().getFkUserId());
			TransferObject transferObject = new TransferObject(TransferType.PLAYERACTION);
			transferObject.setPlayerStatus(PlayerStatus.READY);

			serverSender.send(serverConnection, transferObject);
		}
	}

	/**
	 * This method returns the color for the player
	 * 
	 * @param gameId
	 * @return color
	 * 
	 * @author Ali
	 */
	private String getColorForPlayer(String gameId) {

		GameBoard gameBoard = gameBoards.get(gameId);

		int number = gameBoard.getPlayerHands().size();

		switch (number) {
		case 1:
			return "blue";
		case 2:
			return "grey";
		case 3:
			return "white";
		default:
			return "red";
		}
	}

	/**
	 * This method loads all information which a player needs for his gameBoard
	 * 
	 * @param gameId
	 * @param playerId
	 * @param initGame
	 * @param status
	 *
	 * @author Daniel
	 */
	private void loadGameBoardForPlayer(String gameId, String playerId, boolean initGame, PlayerStatus status) {

		GameBoard gameBoard = gameBoards.get(gameId);

		// fill my GameBoard
		GameBoard myGameBoard = new GameBoard();
		myGameBoard.setStacks(gameBoard.getStacks());
		myGameBoard.setFigures(gameBoard.getFigures());
		myGameBoard.setGame(gameBoard.getGame());

		for (PlayerHand playerHand : gameBoard.getPlayerHands()) {
			if (playerHand.getPlayer().getId().equals(playerId)) {
				myGameBoard.getPlayerHands().add(playerHand);
			}
		}

		TransferObject transferObject = new TransferObject(TransferType.INITGAMEBOARD);

		if (!initGame) {
			transferObject.setTransferType(TransferType.MAKEAMOVERESULT);
		}

		transferObject.setTransferObject(myGameBoard);
		transferObject.setPlayerStatus(status);

		serverSender.send(serverConnection, transferObject);
	}

	/**
	 * This method locates the target location for a specific figure with help
	 * of the played card.
	 * 
	 * @param selectedFigure
	 * @param selectedCard
	 * @param tileStacks
	 * @param oldAimPosition
	 * @return aim position
	 */
	private int locatePosition(Figure selectedFigure, Card selectedCard, List<TileStack> tileStacks,
			int oldAimPosition) {

		int startPosition = oldAimPosition;
		int aimPosition = 0;

		for (int i = startPosition + 1; i < 55; i++) {

			TileStack stack = tileStacks.get(i);

			// check if water
			if (stack.isWater()) {
				continue;
			}

			// check if land, stop move
			if (stack.isLand()) {
				aimPosition = stack.getPosition();
				break;
			}

			// check if aim
			Tile tile = stack.getTiles().get(0);
			if (tile.getTileColor().toString().equals(selectedCard.getCardColor().toString())) {
				aimPosition = stack.getPosition();
				break;
			}
		}

		return aimPosition;
	}

	/**
	 * This method checks if the User is existing, if yes it makes the login of
	 * the User, if not a failed message is returned
	 * 
	 * @param loginData
	 * 
	 * @author Daniel
	 */
	private void login(UserData loginData) {

		UserDaoImpl userDaoImpl = new UserDaoImpl();

		User myUser = null;

		List<User> users = (List<User>) userDaoImpl.readAll();

		for (User user : users) {
			if (user.getName().equals(loginData.getName()) && user.getPassword().equals(loginData.getPassword())) {
				myUser = user;
			}
		}

		TransferObject transferObject = new TransferObject();

		if (myUser != null) {

			// login successfully -> sending player infos and all games

			List<Game> savedGames = getSaveGames(myUser);
			List<Game> newGames = getNewGames();

			serverConnection.setUser(myUser);
			userSeverConnections.put(myUser.getId(), serverConnection);

			LoginResult loginResult = new LoginResult(myUser, savedGames, newGames);

			transferObject.setTransferType(TransferType.LOGIN_SUCCESS);
			transferObject.setTransferObject(loginResult);
		} else {

			// user not existing -> error message

			transferObject.setTransferType(TransferType.LOGIN_FAIL);
		}

		serverSender.send(serverConnection, transferObject);
	}

	/**
	 * This method calls the functionality to make a move
	 * 
	 * @param makeAMove
	 * 
	 * @author Daniel
	 */
	private void makeAMove(MakeAMove makeAMove) {

		List<String> selectedCardsId = new ArrayList<>();
		selectedCardsId.add(makeAMove.getSelectedCard());

		doPlayAction(PlayerStatus.READY, null, true, makeAMove.getGameId(), makeAMove.getPlayerId(), selectedCardsId,
				null, makeAMove.getSelectedFigure(), 0);
	}

	/**
	 * This method checks if the new User to register already exists, if not it
	 * creates the new User
	 * 
	 * @param registerData
	 * 
	 * @author Daniel
	 */
	private void register(UserData registerData) {

		UserDaoImpl userDaoImpl = new UserDaoImpl();

		boolean alreadyExists = false;

		List<User> users = (List<User>) userDaoImpl.readAll();
		for (User user : users) {
			if (user.getName().equals(registerData.getName())) {
				alreadyExists = true;
			}
		}

		if (alreadyExists == true) {

			// registering failed -> error message

			TransferObject transferObject = new TransferObject(TransferType.REGISTER_FAIL);
			serverSender.send(serverConnection, transferObject);
		} else {

			// successfull

			// create new user
			User myUser = new User();
			myUser.setName(registerData.getName());
			myUser.setPassword(registerData.getPassword());
			myUser.setHighscore(0);
			myUser.setVictories(0);
			userDaoImpl.create(myUser);

			TransferObject transferObject = new TransferObject(TransferType.REGISTER_SUCCESS);

			serverSender.send(serverConnection, transferObject);
		}
	}

	/**
	 * This method sends the intermediateResult to the player
	 * 
	 * @param intermediateResult
	 * @param playerStatus
	 * 
	 * @author Daniel
	 */
	private void sendIntermediateResult(IntermediateResult intermediateResult, PlayerStatus playerStatus) {

		TransferObject transferObject = new TransferObject(TransferType.INTERMEDIATERESULT);
		transferObject.setPlayerStatus(playerStatus);
		transferObject.setTransferObject(intermediateResult);

		serverSender.send(serverConnection, transferObject);
	}

	/**
	 * This method sends chat messages to all players
	 * 
	 * @param message
	 * 
	 * @author Daniel
	 */
	private void sendMessage(String message) {

		TransferObject transferObject = new TransferObject(TransferType.MESSAGE);
		transferObject.setTransferObject(message);

		serverSender.sendAll(userSeverConnections, transferObject);
	}
}
