package ch.fhnw.peanutfamily.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import ch.fhnw.peanutfamily.common.TransferObject;
import ch.fhnw.peanutfamily.common.TransferObject.TransferType;

/**
 * The ServerManager creates the socket connection to the user, stores the connection and creates
 * a own listener for him
 * 
 * @author David
 *
 */
public class ServerManager implements Runnable {

	private int port = 9876;
	private Socket socket = null;
	private ServerSocket serverSocket = null;

	/**
	 * This method handles the whole socket creation of a new user
	 */
	@Override
	public void run() {

		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			System.err.println("Could not listen on port: " + port);
			System.exit(1);
		}

		System.out.println("Server running...");

		try {
			while (true) {
				socket = serverSocket.accept();

				// new ServerConnection
				ServerConnection connection = new ServerConnection(socket);

				// create new ServerListener
				Thread t = new Thread(new ServerListener(connection));
				t.start();

				// informs the user about a successful connection
				TransferObject o = new TransferObject(TransferType.CONNECTION_ESTABLISHED);
				connection.getObjectOutputStream().writeObject(o);
			}
		} catch (IOException e) {
			System.err.println("Accept failed.");
		}
	}
}
