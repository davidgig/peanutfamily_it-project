package ch.fhnw.peanutfamily.resources;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The LoggerControl creates and implements the Logger which will Log all Programm steps into the LogFile. 
 * The Level FINE is a message level providing tracing information.
 *
 * @author Roger
 *
 */
public class LoggerControl {
	
	private Logger logger;
	
	/**
	 * Constructor initialising the LoggerControl
	 * 
	 * @author Roger
	 */
	public LoggerControl() {
		logger = Logger.getLogger("TemplateLogger");
		logger.setLevel(Level.FINE);
		
		try {			
			Handler loggerHandler = new FileHandler("./src/ch/fhnw/peanutfamily/resources/LogFile.log");
			loggerHandler.setLevel(Level.FINE);
			logger.addHandler(loggerHandler);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public Logger getLogger() {
		return logger;
	}
}


