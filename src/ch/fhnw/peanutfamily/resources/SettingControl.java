package ch.fhnw.peanutfamily.resources;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Setting Control contains the methods to set the settings of the different
 * Language and stores them into a file called "Settings". It also contains the
 * method of "Save" which will save the selected settings to the file.
 * 
 * @author roger
 *
 */

public class SettingControl {

	private Properties settings;
	private Logger logger;

	/**
	 * Constructor creating the SettingControl
	 * 
	 * @author roger
	 */
	public SettingControl() {

		logger = PropertyControl.getPropertyControl().getLoggerControl().getLogger();

		settings = new Properties();
		InputStream inputstream = null;

		try {
			inputstream = new FileInputStream("./src/ch/fhnw/peanutfamily/resources/Settings.cfg");
			settings.load(inputstream);
			logger.config("Default Settings loaded");
		} catch (IOException e) {
			logger.severe("Error by loading settings");
		} finally {
			try {
				inputstream.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * This method saves the current settings
	 * 
	 * @author roger
	 */
	public void Save() {
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream("./src/ch/fhnw/peanutfamily/resources/Settings.cfg");
			settings.store(outputStream, null);
			logger.config("settings saved");
		} catch (IOException e) {
			logger.severe("Error by saving settings");
		} finally {
			try {
				outputStream.close();
			} catch (IOException e) {
			}
		}
	}

	// getters/setters
	
	public String getSettings(String name) {
		return settings.getProperty(name);
	}

	public void setLocalOption(String name, String value) {
		settings.setProperty(name, value);
	}
}
