package ch.fhnw.peanutfamily.resources;

import ch.fhnw.peanutfamily.client.communication.ClientManager;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The ClientLauncher start the client application
 * 
 * @author David
 *
 */
public class ClientLauncher extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * The start method initialises the PropertyControl and starts the ClientManager
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		// create propertyControl
		PropertyControl propertyControl = PropertyControl.getPropertyControl();
		propertyControl.init();

		// get settingControl
		SettingControl settingControl = propertyControl.getSettingControl();

		// starts the ClientManager
		Thread t = new Thread(new ClientManager(settingControl, primaryStage));
		t.start();
	}
}
