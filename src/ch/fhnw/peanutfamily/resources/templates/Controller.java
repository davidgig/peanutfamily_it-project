package ch.fhnw.peanutfamily.resources.templates;

/**
 * Controller Template
 * 
 * @author David
 *
 * @param <V> View
 * @param <M> Model
 */
public abstract class Controller<V, M> {

	protected V view;
	protected M model;

	protected Controller(V view, M model) {
		this.view = view;
		this.model = model;
	}
}
