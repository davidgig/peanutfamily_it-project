package ch.fhnw.peanutfamily.resources.templates;

import javafx.stage.Stage;

/**
 * View Template
 * 
 * @author David
 *
 * @param <M> Model
 */
public abstract class View<M> {

	protected Stage stage;
	protected M model;

	protected View(Stage stage, M model) {
		this.stage = stage;
		this.model = model;
	}

	/**
	 * This method returns the stagee
	 * 
	 * @return stagee
	 */
	protected Stage getStage() {
		return stage;
	}

	/**
	 * This method handles the whole creation phase of the viewe
	 */
	public void startUI() {
		initUI();
		createUI();
		stage.show();
	}

	/**
	 * This method fills the scene
	 */
	protected abstract void initUI();

	/**
	 * This method fills the stage and starts thew view
	 */
	protected abstract void createUI();
}
