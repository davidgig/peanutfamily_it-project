package ch.fhnw.peanutfamily.resources;

import java.util.logging.Logger;

/**
 * This class initialises the SettingControl, LanguageControl, LoggerControl
 * 
 * @author roger
 *
 */
public class PropertyControl {

	private static PropertyControl propertyControl;

	private SettingControl settingControl;
	private LanguageTranslator languageTranslator;
	private LoggerControl loggerControl;

	private Logger logger = null;

	/**
	 * Creates a singleton of the PropertyControl
	 * 
	 * @return propertyControl
	 * 
	 * @author Roger
	 */
	public static PropertyControl getPropertyControl() {
		if (propertyControl == null) {
			propertyControl = new PropertyControl();
		}
		return propertyControl;
	}

	/**
	 * This method creates the SettingControl, LanguageControl and LoggerControl
	 * 
	 * @author Roger
	 */
	public void init() {
		loggerControl = new LoggerControl();
		logger = getLoggerControl().getLogger();
		logger.config("Logger is configurated");

		settingControl = new SettingControl();
		languageTranslator = new LanguageTranslator(settingControl.getSettings("Language"));
	}

	// getters/setters

	public LoggerControl getLoggerControl() {
		return loggerControl;
	}

	public void setLoggerControl(LoggerControl loggerControl) {
		this.loggerControl = loggerControl;
	}

	public LanguageTranslator getLanguageTranslator() {
		return languageTranslator;
	}

	public void setLanguageTranslator(LanguageTranslator languageTranslator) {
		this.languageTranslator = languageTranslator;
	}

	public SettingControl getSettingControl() {
		return settingControl;
	}

	public void setSettingControl(SettingControl settingControl) {
		this.settingControl = settingControl;
	}
}
