package ch.fhnw.peanutfamily.resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * The Language Translator handles the loading and reading the Language File.
 * The File is loaded with the remarks from properties (DE or EN) and will locate the file. 
 * Also the programm implements the Logger and writes an entry into the LogFile. 
 * 
 * @author Roger
 *
 */
public class LanguageTranslator {
	
	private Logger logger;
	private ResourceBundle resourceBundle;
	protected Locale locale;

	/**
	 * Constructor initialising the LanugageTranslator by the default language 
	 * 
	 * @param language of the Translations
	 * 
	 * @author Roger
	 */
	public LanguageTranslator(String language) {
		
		logger = PropertyControl.getPropertyControl().getLoggerControl().getLogger();

		try {
			FileInputStream fis = 
					new FileInputStream("./src/ch/fhnw/peanutfamily/resources/Translations_" + language + ".properties");
			resourceBundle = new PropertyResourceBundle(fis);
			logger.config("translation loaded");
		} catch (IOException e) {
			logger.severe("Error by loading translations");
		}
	}
	
	public String getTranslation(String key) {
		return resourceBundle.getString(key);
	}
}
