package ch.fhnw.peanutfamily.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ch.fhnw.peanutfamily.common.Game;

/**
 * The GamDaoImpl contains all MySql statements to access the db
 * 
 * @author Ali
 *
 */
public class GameDaoImpl implements DaoInterface {

	/**
	 * This method creates a new Game
	 */
	@Override
	public void create(Object object) {

		Game game = (Game) object;

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection
					.prepareStatement("INSERT INTO game (id, name, amountOfPlayers, state) VALUES (?, ?, ?, ?)");
			preparedStatement.setString(1, game.getId());
			preparedStatement.setString(2, game.getName());
			preparedStatement.setInt(3, game.getAmountOfPlayers());
			preparedStatement.setInt(4, game.getState());

			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method reads a Game with a given id
	 */
	@Override
	public Object read(String id) {

		Game game = new Game();

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM Game WHERE id=?");
			preparedStatement.setString(1, id);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				game.setId(resultSet.getString("id"));
				game.setName(resultSet.getString("name"));
				game.setAmountOfPlayers(resultSet.getInt("amountOfPlayers"));
				game.setState(resultSet.getInt("state"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}

		return game;
	}

	/**
	 * This method reads all Games
	 */
	@Override
	public List<?> readAll() {

		List<Game> games = new ArrayList<Game>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM game");

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Game game = new Game();
				game.setId(resultSet.getString("id"));
				game.setName(resultSet.getString("name"));
				game.setAmountOfPlayers(resultSet.getInt("amountOfPlayers"));
				game.setState(resultSet.getInt("State"));

				games.add(game);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}

		return games;
	}

	/**
	 * This method deletes a Game by its id
	 */
	@Override
	public void delete(String id) {

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM game WHERE id =?");
			preparedStatement.setString(1, id);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method updates a Game
	 */
	@Override
	public void update(Object object) {

		Game game = (Game) object;

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection
					.prepareStatement("UPDATE game SET name=?, amountOfPlayers=?, state=? WHERE id=?");
			preparedStatement.setString(1, game.getName());
			preparedStatement.setInt(2, game.getAmountOfPlayers());
			preparedStatement.setInt(3, game.getState());
			preparedStatement.setString(4, game.getId());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method closes all connections
	 * 
	 * @param connection
	 * @param preparedStatement
	 */
	private void closeAll(Connection connection, PreparedStatement preparedStatement) {

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
