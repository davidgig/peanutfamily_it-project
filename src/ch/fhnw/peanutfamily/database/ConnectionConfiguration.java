package ch.fhnw.peanutfamily.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * The ConnectionConfiguration contains the connection to 
 * the database
 * 
 * @author Ali
 *
 */
public class ConnectionConfiguration {

	/**
	 * This method return the connection to the database 
	 * !! (has to be adapted by the user who runs the server)
	 * 
	 * @return the Connection
	 * 
	 * @author Ali
	 */
	public static Connection getConnection() {
		Connection connection = null;

		try {
			// !! adapt if needed
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/PeanutFamily?autoReconnect=true&useSSL=false", "root", "");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();{
				return null;
			}
		}
	}
}
