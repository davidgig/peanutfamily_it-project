/**
 * 
 */
package ch.fhnw.peanutfamily.database;

import java.util.List;

/**
 * The DaoInterface contains all the functions which has to be implemented 
 * by each class who wants access to the database
 * 
 * @author Ali
 */
public interface DaoInterface {

	/**
	 * Creates a new object
	 * 
	 * @param the object to create
	 */
	void create(Object object);

	/**
	 * Reads a object by its id
	 * 
	 * @param id of the object
	 * @return the object
	 */
	Object read(String id);

	/**
	 * Reads all objects
	 * 
	 * @return the list of objects
	 */
	List<?> readAll();

	/**
	 * Deletes a objects by its id
	 * 
	 * @param id of the object
	 */
	void delete(String id);

	/**
	 * Updates object
	 * 
	 * @param object to update
	 */
	void update(Object object);
}
