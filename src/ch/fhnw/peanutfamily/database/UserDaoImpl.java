package ch.fhnw.peanutfamily.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ch.fhnw.peanutfamily.common.User;

/**
 * The UserDaoImpl contains all MySql statements to access the db
 * 
 * @author Ali
 *
 */
public class UserDaoImpl implements DaoInterface {

	/**
	 * This method creates a new User
	 */
	@Override
	public void create(Object object) {

		User user = (User) object;

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection
					.prepareStatement("INSERT INTO user (id, name, password, highscore, victories) VALUES (?, ?, ?, ?, ?)");
			preparedStatement.setString(1, user.getId());
			preparedStatement.setString(2, user.getName());
			preparedStatement.setString(3, user.getPassword());
			preparedStatement.setInt(4, user.getHighscore());
			preparedStatement.setInt(5, user.getVictories());
			
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method reads a User with a given id
	 */
	@Override
	public Object read(String id) {

		User user = new User();

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM user WHERE id=?");
			preparedStatement.setString(1, id);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				user.setId(resultSet.getString("id"));
				user.setName(resultSet.getString("name"));
				user.setPassword(resultSet.getString("password"));
				user.setHighscore(resultSet.getInt("highscore"));
				user.setVictories(resultSet.getInt("victories"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}

		return user;
	}

	/**
	 * This method reads all Users
	 */
	@Override
	public List<?> readAll() {

		List<User> users = new ArrayList<User>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM user");

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getString("id"));
				user.setName(resultSet.getString("name"));
				user.setPassword(resultSet.getString("password"));
				user.setHighscore(resultSet.getInt("highscore"));
				user.setVictories(resultSet.getInt("victories"));

				users.add(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}

		return users;
	}

	/**
	 * This method deletes a User by its id
	 */
	@Override
	public void delete(String id) {

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM user WHERE id=?");
			preparedStatement.setString(1, id);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method updates a User
	 */
	@Override
	public void update(Object object) {

		User user = (User) object;

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection
					.prepareStatement("UPDATE user SET name=?, password=?, highscore=?, victories=? WHERE id=?");
			preparedStatement.setString(1, user.getName());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setInt(3, user.getHighscore());
			preparedStatement.setInt(4, user.getVictories());
			preparedStatement.setString(5, user.getId());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method closes all connections
	 * 
	 * @param connection
	 * @param preparedStatement
	 */
	private void closeAll(Connection connection, PreparedStatement preparedStatement) {

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
