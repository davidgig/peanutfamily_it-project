package ch.fhnw.peanutfamily.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * The CreateTable cleans first the database of old data
 * and creates all tables which are needed
 * 
 * @author Ali
 *
 */
public class CreateTable {
	
	/**
	 * This function drops the game and player data to remove old data
	 * 
	 * @param connection the Connection to the database
	 * 
	 * @author Ali
	 */
	public void clean(Connection connection) {

		try {
			Statement statement = connection.createStatement();

			statement.executeUpdate("DROP TABLE IF EXISTS Game;");
			statement.executeUpdate("DROP TABLE IF EXISTS Player;");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This function creates all needed tables if not already exist
	 * 
	 * @param connection the Connection to the database
	 * 
	 * @author Ali
	 */
	public void createTables(Connection connection) {

		try {
			Statement statement = connection.createStatement();

			// User
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS User (id char(36) NOT NULL PRIMARY KEY,"
					+ "name varchar (255) NOT NULL," + "password varchar(255) NOT NULL," + "highscore INT NOT NULL,"
					+ "victories INT NOT NULL)");

			// Game
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS Game (id char(36) NOT NULL KEY, "
					+ "name varchar (255) NOT NULL," + "amountOfPlayers INT NOT NULL," + "state INT NOT NULL )");

			// Player
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS PLayer (id char(36) NOT NULL KEY, "
					+ "color varchar (255) NOT NULL," + "fkUserId char(36) NOT NULL," + "fkGameId char(36) NOT NULL )");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
