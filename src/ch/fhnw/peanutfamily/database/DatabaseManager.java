package ch.fhnw.peanutfamily.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * The DatabaseManager creates the connection to the database and runs scripts
 * 
 * @author Ali
 *
 */
public class DatabaseManager {

	private Connection connection;
	
	/**
	 * Constructor creating db connection and running scripts
	 */
	public DatabaseManager() {
		
		// create connection
		ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration();
		connection = ConnectionConfiguration.getConnection();
		
		// runs clean and creates scripts
		CreateTable createTable = new CreateTable();
		createTable.clean(connection);
		createTable.createTables(connection);
		
		System.out.println("Database ready");
	}
}
