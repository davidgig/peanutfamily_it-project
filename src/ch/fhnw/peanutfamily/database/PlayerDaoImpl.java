package ch.fhnw.peanutfamily.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ch.fhnw.peanutfamily.common.Player;

/**
 * The PlayerDaoImpl contains all MySql statements to access the db
 * 
 * @author Ali
 *
 */
public class PlayerDaoImpl implements DaoInterface {

	/**
	 * This method creates a new Player
	 */
	@Override
	public void create(Object object) {

		Player player = (Player) object;

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection
					.prepareStatement("INSERT INTO player(id, fkGameId, fkUserId, color) VALUES (?, ?, ?, ?)");
			preparedStatement.setString(1, player.getId());
			preparedStatement.setString(2, player.getFkGameId());
			preparedStatement.setString(3, player.getFkUserId());
			preparedStatement.setString(4, player.getColor());

			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method reads a Player with a given id
	 */
	@Override
	public Object read(String id) {

		Player player = new Player();

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM player WHERE id=?");
			preparedStatement.setString(1, id);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				player.setId(resultSet.getString("id"));
				player.setColor(resultSet.getString("color"));
				player.setFkGameId(resultSet.getString("fkdUserId"));
				player.setFkUserId(resultSet.getString("fkUserId"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}

		return player;
	}

	/**
	 * This method reads all Players
	 */
	@Override
	public List<?> readAll() {

		List<Player> players = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM player");

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Player player = new Player();
				player.setId(resultSet.getString("id"));
				player.setColor(resultSet.getString("color"));
				player.setFkUserId(resultSet.getString("fkUserId"));
				player.setFkGameId(resultSet.getString("fkGameId"));

				players.add(player);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}

		return players;
	}

	/**
	 * This method deletes a Player by its id
	 */
	@Override
	public void delete(String id) {

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM Player WHERE id =?");
			preparedStatement.setString(1, id);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method updates a Player
	 */
	@Override
	public void update(Object object) {

		Player player = (Player) object;

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionConfiguration.getConnection();
			preparedStatement = connection
					.prepareStatement("UPDATE player SET color=?, fkUserId=?, fkGameId=? WHERE id=?");
			preparedStatement.setString(1, player.getColor());
			preparedStatement.setString(2, player.getFkUserId());
			preparedStatement.setString(3, player.getFkGameId());
			preparedStatement.setString(4, player.getId());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(connection, preparedStatement);
		}
	}

	/**
	 * This method closes all connections
	 * 
	 * @param connection
	 * @param preparedStatement
	 */
	private void closeAll(Connection connection, PreparedStatement preparedStatement) {

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
